﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using 2 thư viện thiết kế class metadata
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace DAweb.Models
{
    [MetadataTypeAttribute(typeof(tblSanPhamMetadata))]
    public partial class tblSanPham
    {
        internal sealed class tblSanPhamMetadata
        {
            [Display(Name = "Mã Sản Phầm ")]//Thuộc tính Display dùng để đặt tên lại cho cột
            public int MaSP { get; set; }
            [Display(Name = "Tên Sản Phẩm")]//Thuộc tính Display dùng để đặt tên lại cho cột
            [Required(ErrorMessage = "Vui lòng nhập dữ liệu cho trường này.")] //Kiểm tra rổng
            public string TenSP { get; set; }
            [Display(Name = "Thông Tin ")]//Thuộc tính Display dùng để đặt tên lại cho cột
            [Required(ErrorMessage = "Vui lòng nhập dữ liệu cho trường này.")] //Kiểm tra rổng
            public string ThongTin { get; set; }
            [Display(Name = "Giá Tiền")]//Thuộc tính Display dùng để đặt tên lại cho cột
            [Required(ErrorMessage = "Vui lòng nhập dữ liệu cho trường này.")] //Kiểm tra rổng
            public Nullable<double> GiaTien { get; set; }
            [Display(Name = "Số Lượng Tồn Kho")]//Thuộc tính Display dùng để đặt tên lại cho cột
            [Required(ErrorMessage = "Vui lòng nhập dữ liệu cho trường này.")] //Kiểm tra rổng
            public Nullable<int> SoLuongTonKho { get; set; }
            [Display(Name = "Mã Loại Sản Phẩm ")]//Thuộc tính Display dùng để đặt tên lại cho cột
            public Nullable<int> MaLoaiSP { get; set; }
            [Display(Name = "Ảnh Minh Họa ")]//Thuộc tính Display dùng để đặt tên lại cho cột
            public string AnhMinhHoa { get; set; }
            [Display(Name = "Trạng Thái ")]//Thuộc tính Display dùng để đặt tên lại cho cột
            public bool TrangThai { get; set; }
            [Display(Name = "SIZE")]//Thuộc tính Display dùng để đặt tên lại cho cột
            [Required(ErrorMessage = "Vui lòng nhập dữ liệu cho trường này.")] //Kiểm tra rổng
            public string SIZE { get; set; }
        }
    }
}