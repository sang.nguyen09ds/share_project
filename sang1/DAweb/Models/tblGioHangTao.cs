﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAweb.Models
{
    public class tblGioHangTao
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        public string TenTaiKhoan { get; set; }
        public int MaSP { get; set; }
        public int SoLuong { get; set; }
        public string AnhDaiDien { get; set; }
        public double DonGia { get; set; }
        public Nullable<decimal> ThanhTien { get; set; }
        public string TenSanPham { get; set; }
        public string SiZe { get; set; }
        public double dThanhTien { get { return (double)(SoLuong * DonGia); } }

        public virtual tblSanPham tblSanPham { get; set; }
        public virtual tblTaiKhoan tblTaiKhoan { get; set; }
        // hàm tạo giỏ hàng 
        public tblGioHangTao(int MASP)
        {
            MaSP = MASP;
            tblSanPham sp = db.tblSanPhams.Single(n => n.MaSP == MASP);
            TenSanPham = sp.TenSP;
            AnhDaiDien = sp.AnhMinhHoa;
            SiZe = sp.SIZE;
            DonGia = double.Parse(sp.GiaTien.ToString());
            SoLuong = 1;
        }
    }
}