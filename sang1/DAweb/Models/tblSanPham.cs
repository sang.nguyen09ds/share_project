//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAweb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSanPham
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblSanPham()
        {
            this.tblCTHoaDons = new HashSet<tblCTHoaDon>();
            this.tblGioHangs = new HashSet<tblGioHang>();
        }
    
        public int MaSP { get; set; }
        public string TenSP { get; set; }
        public string ThongTin { get; set; }
        public Nullable<double> GiaTien { get; set; }
        public Nullable<int> SoLuongTonKho { get; set; }
        public Nullable<int> MaLoaiSP { get; set; }
        public string AnhMinhHoa { get; set; }
        public bool TrangThai { get; set; }
        public string SIZE { get; set; }
        public Nullable<bool> SPMoi { get; set; }
        public Nullable<double> GiaNhap { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblCTHoaDon> tblCTHoaDons { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblGioHang> tblGioHangs { get; set; }
        public virtual tblLoaiSanPham tblLoaiSanPham { get; set; }
    }
}
