﻿using System.Web;
namespace DAweb.Models
{
    using System;
    using System.Collections.Generic;

    public partial class Image1
    {
        public long ID { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public Nullable<long> ProductID { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }
    }
    public partial class ImagePost
    {
        public long ID { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public Nullable<long> PostID { get; set; }
        public HttpPostedFileBase Thumbnail { get; set; }
    }
}