﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
namespace DAweb.Controllers
{
    public class DanhMucSanPhamController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // GET: DanhMucSanPham
        public ActionResult Index()
        {

            return View();
        }
        // danh mục  hiển thị các Loại Sản phẩm
        public ActionResult DanhSachLoaiSanPhamPartial()
        {
            var ds = db.DanhMucs.ToList();
            return View(ds);
        }
        //  text thu coi 
        public ActionResult _PartialPage1()
        {
            return View();
        }

    }
}