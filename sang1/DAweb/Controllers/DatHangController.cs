﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Controllers;
using DAweb.Models;
namespace DAweb.Controllers
{
    public class DatHangController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // GET: DatHang
        public List<tblGioHangTao> LayGioHang()
        {
            List<tblGioHangTao> lstGioHang = Session["tblGioHangTao"] as List<tblGioHangTao>;
            if (lstGioHang == null)
            {
                // nếu giỏ hàng chưa tồn tại thì mình tiến hành  khởi tao list giỏ hàng (Session GioHang)
                lstGioHang = new List<tblGioHangTao>();
                Session["tblGioHangTao"] = lstGioHang;
            }
            return lstGioHang;
        }
        // thêm giỏ hàng 
        public ActionResult ThemGioHang(int MASP, string strURL)
        {
            tblSanPham sp = db.tblSanPhams.SingleOrDefault(n => n.MaSP == MASP);
            if (sp == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            //Lấy ra session giỏ hàng
            List<tblGioHangTao> lstGioHang = LayGioHang();
            //Kiểm tra sách này đã tồn tại trong session[giohang] chưa
            tblGioHangTao sanpham = lstGioHang.Find(n => n.MaSP == MASP);
            if (sanpham == null)
            {
                sanpham = new tblGioHangTao(MASP);
                //Add sản phẩm mới thêm vào list
                lstGioHang.Add(sanpham);
                return Redirect(strURL);
            }
            else
            {
                sanpham.SoLuong++;
                return Redirect(strURL);
            }
        }
        // cập nhập giỏ hàng 
        public ActionResult CapNhapGioHang(int MASP, FormCollection f)
        {
            tblSanPham sach = db.tblSanPhams.SingleOrDefault(n => n.MaSP == MASP);
            if (sach == null)
            {
                Response.StatusCode = 404;
            }
            List<tblGioHangTao> lstGioHang = LayGioHang();
            tblGioHangTao sanpham = lstGioHang.SingleOrDefault(n => n.MaSP == MASP);
            if (sanpham != null)
            {
              
                    sanpham.SoLuong = int.Parse(f["txtSoLuong"].ToString());
                
                

            }
            return RedirectToAction("GioHang");
        }
        public double TongTien()
        {
            double dTongTien = 0;
            List<tblGioHangTao> lstGioHang = Session["tblGioHangTao"] as List<tblGioHangTao>;
            if (lstGioHang != null)
            {
                dTongTien = lstGioHang.Sum(n => n.dThanhTien);
            }
            return dTongTien;
        }
        public ActionResult DatHang()
        {
            return View();
        }
        // chức năng đặt hàng 
        [HttpPost]
        public ActionResult DatHang(FormCollection HD)
        {   // kiểm tra đăng nhập 
            if (Request.Cookies["TaiKhoan"] == null || Request.Cookies["TaiKhoan"].ToString() == "")
            {
                return RedirectToAction("DangNhap", "NguoiDung");
            }
            //Kiểm tra giỏ hàng
            if (Session["tblGioHangTao"] == null)
            {
                RedirectToAction("Index", "Home");
            }
            tblHoaDon ddh = new tblHoaDon();
            string tk = Request.Cookies["TaiKhoan"]["TenTaiKhoan"];

            GioHangController t = new GioHangController();
            List<tblGioHangTao> gh =LayGioHang();
            /// luu ten tài khoản  vô hóa đơn
            db.SaveChanges();
            ddh.TenTaiKhoan = tk;
            ddh.NgayMua = DateTime.Now;
            ddh.TongTien = (Decimal)TongTien();
            ddh.DiaChiGiaoHang = HD["DCGH"].ToString();
            ddh.SDTGiaoHang = HD["SDT"];
            ddh.TrangThai = "Chưa Sử Lý";
            db.tblHoaDons.Add(ddh);

           
            db.SaveChanges();
            // thêm chi tiết đơn hàng 
            List<tblCTHoaDon> dscthd = new List<tblCTHoaDon>();
            foreach (var item in gh)
            {
                tblCTHoaDon ctdh = new tblCTHoaDon();
                ctdh.MaHD = ddh.MaHD;
                ctdh.MaSP = item.MaSP;
                ctdh.SoLuong = item.SoLuong;
                ctdh.DonGia = Convert.ToInt32(item.DonGia);
                ctdh.AnhSanPham = item.AnhDaiDien;
                ctdh.TenSanPham = item.TenSanPham;
                db.tblCTHoaDons.Add(ctdh);
                dscthd.Add(ctdh);
            }
            // tiến hành  trừ số lượng sản phẩm trong bảng sản phẩm 
             foreach( var item in dscthd)
            {
                var sp = db.tblSanPhams.SingleOrDefault(n => n.MaSP == item.MaSP);
                sp.SoLuongTonKho = sp.SoLuongTonKho - item.SoLuong;
            }
            db.SaveChanges();
            gh.Clear();
            return RedirectToAction("Index", "Home");
        }

    }
}