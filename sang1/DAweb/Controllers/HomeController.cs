﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
using PagedList;
using PagedList.Mvc;
namespace DAweb.Controllers
{     
    public class HomeController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();

        // GET: Home
        public ActionResult Index(int? page )
        {

            int pagenumber = (page ?? 1);
            int pageSize = 8;
            var sp = db.tblSanPhams.ToList();
            List<tblSanPham> dssp = new List<tblSanPham>();
            foreach( var item in sp)
            {
                
                if(item.SPMoi==true)
                {
                    dssp.Add(item);
                }
            }
            return View(dssp.ToPagedList(pagenumber, pageSize));       
        }
    }
}