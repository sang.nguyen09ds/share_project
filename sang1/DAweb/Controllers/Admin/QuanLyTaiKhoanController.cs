﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
using System.IO;

namespace DAweb.Controllers.Admin
{
    public class QuanLyTaiKhoanController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // GET: QuanLyTaiKhoan
        [HttpGet]
        public ActionResult QuanLyTaiKhoan()
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            var item = db.tblTaiKhoans.ToList();
            ViewBag.ThongBao = item.Count();
            return View(item);
        }
        // thống kê danh sách tài khoản 
        [HttpGet]
        public ActionResult ThongKeTaiKhoan()
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            var item = db.tblTaiKhoans.ToList();
            ViewBag.ThongBao = item.Count();
            return View(item);
        }
        // không cho admin chinh sửa thông tin tài khoản của người dùng 
        [HttpGet]
        public ActionResult ChinhSuaTK(string TenTaiKhoan)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            // lấy ra đối tượng sách theo mã 
            var tk = db.tblTaiKhoans.SingleOrDefault(n => n.TenTaiKhoan == TenTaiKhoan);
            if (tk == null)
            {
                Response.StatusCode = 404;
            }
            return View(tk);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChinhSuaTK(string TenTaiKhoan, HttpPostedFileBase fileUpload)
        {
            var tk = db.tblTaiKhoans.SingleOrDefault(n => n.TenTaiKhoan == TenTaiKhoan);
            if (tk != null)
            {
                if (fileUpload != null)
                {
                    string hinh = tk.AnhDaiDien;
                    string fullPath = Request.MapPath("~/App_Themes/AnhDaiDienTK/" + hinh);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                    // lưu tên của file 
                    var fileName = Path.GetFileName(fileUpload.FileName);
                    // lưu đường dẫn của file 
                    var path = Path.Combine(Server.MapPath("~/App_Themes/AnhDaiDienTK"), fileName);
                    // kiểm tra hình ảnh đã tồn tại hay chưa 
                    if (System.IO.File.Exists(path))
                    {
                        ViewBag.ThongBao = " Hình ảnh Đã Tồn Tại";
                    }
                    else
                    {

                        fileUpload.SaveAs(path);
                        tk.AnhDaiDien = fileUpload.FileName;
                    }
                }
                //Thêm vào cơ sở dữ liệu
                if (ModelState.IsValid)
                {  
                    //Thực hiện cập nhận trong model
                    UpdateModel(tk);
                    if (tk.LaAdmin)
                    {
                        tk.Role = "Admin";
                    }
                    else tk.Role = "User";
                    db.SaveChanges();
                }
            }
            return RedirectToAction("QuanLyTaiKhoan");
        }
        // xóa tài khoản 
        //Xóa sản phẩm
        [HttpGet]
        public ActionResult XoaTaiKhoan(string TenTaiKhoan)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            //Lấy ra đối tượng sách theo mã 
            tblTaiKhoan tk = db.tblTaiKhoans.SingleOrDefault(n => n.TenTaiKhoan == TenTaiKhoan);
            if (tk == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(tk);
        }
        [HttpPost, ActionName("XoaTaiKhoan")]

        public ActionResult XacNhanXoa(string TenTaiKhoan)
        {
            tblTaiKhoan tk = db.tblTaiKhoans.SingleOrDefault(n => n.TenTaiKhoan == TenTaiKhoan);
            if (tk == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            string hinh = tk.AnhDaiDien;
            string fullPath = Request.MapPath("~/App_Themes/AnhDaiDienTK/" + hinh);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
            tk.LaAdmin = false;
            tk.TrangThai = false;
            db.tblTaiKhoans.Remove(tk);
            db.SaveChanges();
            return RedirectToAction("QuanLyTaiKhoan");
        }
    }
}