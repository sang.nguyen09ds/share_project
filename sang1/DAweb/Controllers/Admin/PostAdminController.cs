﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
using System.IO;
using PagedList;
using PagedList.Mvc;
namespace DAweb.Controllers.Admin
{
    public class PostAdminController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // GET: PostAdmin
        [HttpGet]
        public ActionResult CreatePost()
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreatePost(POST sp, HttpPostedFileBase fileUpload)
        {
            // đưa dữ liệu vào dropdownlist
            if (fileUpload == null)
            {
                ViewBag.ThongBao = "Chọn Hình Ảnh";
            }
            else if (fileUpload != null)
            {
                // thêm vào cơ sở dữ liệu 
                if (ModelState.IsValid)
                {     // lưu tên của file 
                    var fileName = Path.GetFileName(fileUpload.FileName);
                    // lưu đường dẫn của file 
                    var path = Path.Combine(Server.MapPath("~/App_Themes/AnhPost"), fileName);
                    // kiểm tra hình ảnh đã tồn tại hay chưa 
                    if (System.IO.File.Exists(path))
                    {
                        ViewBag.ThongBao = " Hình ảnh Đã Tồn Tại";
                    }
                    else
                    {
                        fileUpload.SaveAs(path);
                        sp.Thumbnail = fileUpload.FileName;
                        db.POSTs.Add(sp);
                        db.SaveChanges();
                        return RedirectToAction("DanhSachPostAdmin");
                    }
                }
            }

            return View();
        }
        public ActionResult PostAdmin(int ?page)
        {
            int pagenumber = (page ?? 1);
            int pageSize = 5;
            return View(db.POSTs.ToList().OrderBy(n => n.id).ToPagedList(pagenumber, pageSize));
        }
        public ActionResult DanhSachPostAdmin()
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            return View();
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            var pr = db.POSTs.First(m => m.id == id);
            return View(pr);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int id, HttpPostedFileBase fileUpload)
        {          
            var sp = db.POSTs.SingleOrDefault(n => n.id == id);
            if (sp != null)
            {
                if (fileUpload != null)
                {
                    string hinh = sp.Thumbnail;
                    string fullPath = Request.MapPath("~/App_Themes/AnhPost/" + hinh);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                    // lưu tên của file 
                    var fileName = Path.GetFileName(fileUpload.FileName);
                    // lưu đường dẫn của file 
                    var path = Path.Combine(Server.MapPath("~/App_Themes/AnhPost"), fileName);


                    // kiểm tra hình ảnh đã tồn tại hay chưa 

                    if (System.IO.File.Exists(path))
                    {
                        ViewBag.ThongBao = " Hình ảnh Đã Tồn Tại";
                        //return View();
                    }
                    else
                    {
                        fileUpload.SaveAs(path);
                        sp.Thumbnail = fileUpload.FileName;
                    }
                }
            }
            if (ModelState.IsValid)
            {
                //Thực hiện cập nhập trong model
                UpdateModel(sp);
                db.SaveChanges();
                ViewBag.ThongBao = " Cập Nhập Dữ Liệu Thanh Thành Công ";
            }
            return RedirectToAction("DanhSachPostAdmin");

        }
        public ActionResult Xoa(int id)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            POST pr = db.POSTs.SingleOrDefault(n => n.id == id);
            if (pr == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            db.POSTs.Remove(pr);
            db.SaveChanges();

            return RedirectToAction("DanhSachPostAdmin", "PostAdmin");

        }
    }
}