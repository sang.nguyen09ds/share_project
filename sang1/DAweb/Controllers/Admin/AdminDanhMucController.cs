﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
namespace DAweb.Controllers.Admin
{
    public class AdminDanhMucController : Controller
     {


        // GET: AdminDanhMuc
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // thêm danh mục cha '

        [HttpGet]
        public ActionResult ThemDanhMucCha()
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ThemDanhMucCha(FormCollection f, DanhMuc dmCha)
        {
          
            string sTenDMCha = f["TenDM"];
            dmCha.TenDanhMucCha = sTenDMCha;
            db.DanhMucs.Add(dmCha);
            db.SaveChanges();
            return RedirectToAction("ThemDanhMucCha", "AdminDanhMuc");
        }
        // tiến hành sửa  danh mục cha 
        [HttpGet]
        public ActionResult ChinhSuaDanhMucCha(int MaDanhMuc)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            // lấy ra đối tượng sách theo mã 
            DanhMuc DMcha = db.DanhMucs.SingleOrDefault(n => n.MaDanhMuc == MaDanhMuc);
            if (DMcha == null)
            {
                Response.StatusCode = 404;
            }
            //Đưa dữ liệu vào dropdownlist
         
            return View(DMcha);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChinhSuaDanhMucCha( FormCollection f)
        {
              int iMaDanhMuc =int.Parse( f["MaDanhMuc"].ToString());
             string sTenDanhMuc = f["TenDanhMucCha"];
            var DMCha = db.DanhMucs.SingleOrDefault(n => n.MaDanhMuc == iMaDanhMuc);
            DMCha.TenDanhMucCha = sTenDanhMuc;
            if(DMCha==null)
            {
                Response.StatusCode = 404;
            }
            //Thêm vào cơ sở dữ liệu
            if (ModelState.IsValid)
            {
                //Thực hiện cập nhập trong model
                UpdateModel(DMCha);
                db.SaveChanges();
                ViewBag.ThongBao = "Cập Nhập Dữ Liệu Thanh Thành Công ";
            }

            //Đưa dữ liệu vào dropdownlist
            return RedirectToAction("ThemDanhMucCha", "AdminDanhMuc");
        }
        [HttpGet]
        public ActionResult ChinhSuaDanhMuc(int MaLoaiSP)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            // lấy ra đối tượng sách theo mã 
            tblLoaiSanPham DM = db.tblLoaiSanPhams.SingleOrDefault(n => n.MaLoaiSP == MaLoaiSP);
            if (DM == null)
            {
                Response.StatusCode = 404;
            }
            //Đưa dữ liệu vào dropdownlist
            ViewBag.MaDanhMuc = new SelectList(db.DanhMucs.ToList().OrderBy(n => n.TenDanhMucCha), "MaDanhMuc", "TenDanhMucCha", DM.MaDanhMuc);
            return View(DM);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChinhSuaDanhMuc(tblLoaiSanPham f)
        {
            //Thêm vào cơ sở dữ liệu
            if (ModelState.IsValid)
            {
                //Thực hiện cập nhập trong model
                db.Entry(f).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                ViewBag.ThongBao = "Cập Nhập Dữ Liệu Thanh Thành Công ";
            }
            //Đưa dữ liệu vào dropdownlist
            ViewBag.MaDanhMuc = new SelectList(db.DanhMucs.ToList().OrderBy(n => n.TenDanhMucCha), "MaDanhMuc", "TenDanhMucCha", f.MaDanhMuc);
            return RedirectToAction("ThemDanhMuc", "AdminDanhMuc");
        }
        [HttpGet]
        public ActionResult ThemDanhMuc()
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            ViewBag.MaDanhMuc = new SelectList(db.DanhMucs.ToList().OrderBy(n => n.TenDanhMucCha), "MaDanhMuc", "TenDanhMucCha");
            return View();
        }
        [HttpPost]
        public ActionResult ThemDanhMuc(FormCollection f, tblLoaiSanPham dm)
        {
            ViewBag.MaDanhMuc = new SelectList(db.DanhMucs.ToList().OrderBy(n => n.TenDanhMucCha), "MaDanhMuc", "TenDanhMucCha");
            string sTenDM = f["TenDM"];
            dm.TenLoaiSP = sTenDM;
            db.tblLoaiSanPhams.Add(dm);
            db.SaveChanges();
            return RedirectToAction("ThemDanhMuc", "AdminDanhMuc");
        }
        public ActionResult DanhMucAdmin()
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            CategoryModel cate = new CategoryModel();
            cate.catModel = db.tblSanPhams.ToList();
            return View(cate);
        }
        // show danh mục Cha 
        public ActionResult ShowDMAdminCha()
        {
            var model = db.DanhMucs.ToList();
            return View(model);
        }
        public ActionResult ShowDMAdmin()
        {
            var model = db.tblLoaiSanPhams;
            return PartialView("ShowDMAdmin", model);
        }
        // tiến hành xóa danh mục cha 
        [HttpGet]
        public ActionResult XoaDMCha(int MaDanhMuc)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            //Lấy ra đối tượng sách theo mã 
            DanhMuc L = db.DanhMucs.SingleOrDefault(n => n.MaDanhMuc == MaDanhMuc);
            if (L == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(L);
        }
        [HttpPost, ActionName("XoaDMCha")]

        public ActionResult XacNhanXoaDanhMucCha(int MaDanhMuc)
        {   
            DanhMuc DMCha = db.DanhMucs.SingleOrDefault(n => n.MaDanhMuc == MaDanhMuc);
            if (DMCha == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            List<tblLoaiSanPham> LoaiSP = db.tblLoaiSanPhams.Where(n => n.MaDanhMuc == MaDanhMuc).ToList();
            foreach (var item in LoaiSP)
            {
                var sp = db.tblSanPhams.ToList().Where(n=>n.MaLoaiSP== item.MaLoaiSP);
                     foreach( var i in sp)
                { i.TrangThai = false;
                }
                item.TrangThai = false;
                db.tblSanPhams.RemoveRange(sp);
            }             
            db.tblLoaiSanPhams.RemoveRange(LoaiSP);
            db.DanhMucs.Remove(DMCha);
            db.SaveChanges();
            return RedirectToAction("ThemDanhMucCha");
        }
        //Xóa  Danh Mục Con 
        [HttpGet]
        public ActionResult XoaDM(int MaLoaiSP)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            //Lấy ra đối tượng sách theo mã 
            tblLoaiSanPham L = db.tblLoaiSanPhams.SingleOrDefault(n => n.MaLoaiSP == MaLoaiSP);
            if (L == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(L);
        }
        [HttpPost, ActionName("XoaDM")]

        public ActionResult XacNhanXoa(int MaLoaiSP)
        {
            tblLoaiSanPham sp = db.tblLoaiSanPhams.SingleOrDefault(n => n.MaLoaiSP == MaLoaiSP);
            if (sp == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            List<tblSanPham> dssp = db.tblSanPhams.Where(n=>n.MaLoaiSP==sp.MaLoaiSP).ToList();

              foreach  (var item in dssp)
            {
                    item.TrangThai = false;
                
                    db.tblSanPhams.Remove(item);
            }
            sp.TrangThai = false;
            db.tblLoaiSanPhams.Remove(sp);
            db.SaveChanges();
            return RedirectToAction("ThemDanhMuc");

        }

    }
}