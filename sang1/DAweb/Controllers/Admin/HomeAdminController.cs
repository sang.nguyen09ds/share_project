﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace DAweb.Controllers.Admin
{
    public class HomeAdminController : Controller
    {
        // GET: HomeAdmin
        public ActionResult Index()
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            return View();
        }
        private object GetUserCookie()
        {
            string cookieName = FormsAuthentication.FormsCookieName; //Find cookie name
            HttpCookie authCookie = HttpContext.Request.Cookies[cookieName]; //Get the cookie by it's name

            if (authCookie != null)
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value); //Decrypt it
                var UserName = ticket.Name; //You have the UserName!
                if (UserName != null)
                {
                    return UserName;
                }

            }

            return null;
        }
    }
}