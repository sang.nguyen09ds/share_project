﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
using System.IO;

namespace DAweb.Controllers.Admin
{
    public class AdminSlideShowController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();

        // GET: AdminSlideShow
        [HttpGet]
        public ActionResult AdminSlideShow(int id = 1)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            SildeShow sl = db.SildeShows.SingleOrDefault(n => n.id == 1);
            if (sl == null)
            {
                Response.StatusCode = 404;
            }
            return View(sl);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AdminSlideShow(FormCollection f, HttpPostedFileBase fileImgSlide1, HttpPostedFileBase fileImgSlide2, HttpPostedFileBase fileImgSlide3)
        {
            long sid = long.Parse(f["id"]);
            var sp = db.SildeShows.SingleOrDefault(n => n.id == sid);
            if (sp != null)
            {
                if (fileImgSlide1 != null)
                {                                       
                    string hinh = sp.ImgSlide1;
                    string fullPath = Request.MapPath("~/App_Themes/images/slideshow/" + hinh);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                    // lưu tên của file 
                    var fileName = Path.GetFileName(fileImgSlide1.FileName);
                    // lưu đường dẫn của file 
                    var path = Path.Combine(Server.MapPath("~/App_Themes/images/slideshow"), fileName);
                    // kiểm tra hình ảnh đã tồn tại hay chưa 

                    if (System.IO.File.Exists(path))
                    {
                        ViewBag.ThongBao = " Hình ảnh Đã Tồn Tại";
                        //return View();
                    }
                    else
                    {
                        fileImgSlide1.SaveAs(path);
                        sp.ImgSlide1 = fileImgSlide1.FileName;
                    }
                }

                if (fileImgSlide2 != null)
                {
                    string hinh = sp.ImgSdile2;
                    string fullPath = Request.MapPath("~/App_Themes/images/slideshow/" + hinh);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                    // lưu tên của file 
                    var fileName = Path.GetFileName(fileImgSlide2.FileName);
                    // lưu đường dẫn của file 
                    var path = Path.Combine(Server.MapPath("~/App_Themes/images/slideshow"), fileName);
                    // kiểm tra hình ảnh đã tồn tại hay chưa 

                    if (System.IO.File.Exists(path))
                    {
                        ViewBag.ThongBao = " Hình ảnh Đã Tồn Tại";
                        //return View();
                    }
                    else
                    {
                        fileImgSlide2.SaveAs(path);
                        sp.ImgSdile2 = fileImgSlide2.FileName;
                    }
                }
                if (fileImgSlide3 != null)
                {
                    string hinh = sp.ImgSlide3;
                    string fullPath = Request.MapPath("~/App_Themes/images/slideshow/" + hinh);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                    // lưu tên của file 
                    var fileName = Path.GetFileName(fileImgSlide3.FileName);
                    // lưu đường dẫn của file 
                    var path = Path.Combine(Server.MapPath("~/App_Themes/images/slideshow"), fileName);
                    // kiểm tra hình ảnh đã tồn tại hay chưa 

                    if (System.IO.File.Exists(path))
                    {
                        ViewBag.ThongBao = " Hình ảnh Đã Tồn Tại";
                        //return View();
                    }
                    else
                    {
                        fileImgSlide3.SaveAs(path);
                        sp.ImgSlide3 = fileImgSlide3.FileName;
                    }
                }
            }
            if (ModelState.IsValid)
            {
                //Thực hiện cập nhập trong model
                UpdateModel(sp);
                db.SaveChanges();
                ViewBag.ThongBao = " Cập Nhập Dữ Liệu Thanh Thành Công ";
                //Thêm vào cơ sở dữ liệu
                //Đưa dữ liệu vào dropdownlist   
            }
            return RedirectToAction("Index", "HomeAdmin");

        }
    }
}