﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
using System.IO;

namespace DAweb.Controllers.Admin
{
    public class QuangCaoController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // GET: QuangCao
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult QuangCao(int id=1)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            GioiThieu d = db.GioiThieux.SingleOrDefault(n => n.id == id);
            if (d== null)
            {
                Response.StatusCode = 404;
            }
            return View(d);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult QuangCao( FormCollection f, HttpPostedFileBase fileUploadAnhNen, HttpPostedFileBase fileUploadAnh)
        {
            long sid = long.Parse( f["id"]);
            var sp = db.GioiThieux.SingleOrDefault(n => n.id == sid);
            if (sp != null)
            {
                if (fileUploadAnhNen != null)
                {
                    string hinh = sp.HinhAnhNen;
                    string fullPath = Request.MapPath("~/App_Themes/images/img-banner/" + hinh);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                    // lưu tên của file 
                    var fileName = Path.GetFileName(fileUploadAnhNen.FileName);
                    // lưu đường dẫn của file 
                    var path = Path.Combine(Server.MapPath("~/App_Themes/images/img-banner"), fileName);
                    // kiểm tra hình ảnh đã tồn tại hay chưa 

                    if (System.IO.File.Exists(path))
                    {
                        ViewBag.ThongBao = " Hình ảnh Đã Tồn Tại";
                        //return View();
                    }
                    else
                    {
                        fileUploadAnhNen.SaveAs(path);
                        sp.HinhAnhNen = fileUploadAnhNen.FileName;
                    }
                }

                if (fileUploadAnh != null)
                {
                    string hinh = sp.HinhAnhNen;
                    string fullPath = Request.MapPath("~/App_Themes/images/thum/" + hinh);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                    // lưu tên của file 
                    var fileName = Path.GetFileName(fileUploadAnh.FileName);
                    // lưu đường dẫn của file 
                    var patha = Path.Combine(Server.MapPath("~/App_Themes/images/thum"), fileName);
                    // kiểm tra hình ảnh đã tồn tại hay chưa 

                    if (System.IO.File.Exists(patha))
                    {
                        ViewBag.ThongBao = " Hình ảnh Đã Tồn Tại";
                        //return View();
                    }
                    else
                    {
                        fileUploadAnh.SaveAs(patha);
                        sp.HinhAnh = fileUploadAnh.FileName;
                    }
                }
            }
            if (ModelState.IsValid)
            {
                //Thực hiện cập nhập trong model
                UpdateModel(sp);
                db.SaveChanges();
                ViewBag.ThongBao = " Cập Nhập Dữ Liệu Thanh Thành Công ";
                //Thêm vào cơ sở dữ liệu
                //Đưa dữ liệu vào dropdownlist   
            }
            return RedirectToAction("Index", "HomeAdmin");

        }
    }
}