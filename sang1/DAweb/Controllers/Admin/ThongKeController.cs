﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
using PagedList;
using PagedList.Mvc;
using System.IO;
namespace DAweb.Controllers.Admin
{
    public class ThongKeController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // GET: ThongKe
        public ActionResult DanhSachSanPham()
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            return View();
        }
        [HttpGet]
        public ActionResult ChonLoaiSP()
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            var lsp = db.tblLoaiSanPhams.ToList();
            return View(lsp);
        }

        public ActionResult listChonLoaiSP(FormCollection f)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            var sp = db.tblSanPhams.ToList();
            ViewBag.TongSanPham = sp.Count();
           
            int maloai;
            if (f["MaLoaiSP"] == null)
            {
                maloai = 1;
            }
            else
                maloai = int.Parse(f["MaLoaiSP"].ToString());
                List<tblSanPham> lsp = db.tblSanPhams.Where(n => n.MaLoaiSP == maloai).OrderBy(n => n.MaSP).ToList();
                 ViewBag.SoLuong = lsp.Count();
                 ViewBag.TongTien=lsp.Sum(n => n.GiaTien);
            return View(lsp);
        }
        // thống kê doanh thu của các hóa đơn 
        public ActionResult DoanhThu ()
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            double TongTien = 0;
            var cthd = db.tblCTHoaDons.ToList();
            if (cthd == null)
            {
                Response.StatusCode = 404;
            }
            TongTien =(double) cthd.Sum(n => n.SoLuong * n.DonGia);
            ViewBag.TongTien = TongTien;
            double dTongTienSP = 0;
              foreach ( var item in cthd )
               {
                tblSanPham sp = db.tblSanPhams.SingleOrDefault(n => n.MaSP == item.MaSP);
                dTongTienSP+=(double)(sp.GiaNhap*item.SoLuong);
               }
             ViewBag.STienSP = dTongTienSP;
             ViewBag.DT = TongTien - dTongTienSP;
                return View(cthd);
        }
        // thống kê sản phẩm bán chạy nhất 
        public ActionResult SPBanChay(FormCollection f)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            int chiTieu;
            if (f["ChiTieu"] == null)
            {
                chiTieu = 0;
            }
            else
            {
                chiTieu = int.Parse(f["ChiTieu"].ToString());
            }
            var spbc = db.tblSanPhams.ToList();
            List<tblSanPham> dssp = new List<tblSanPham>();
             foreach (var iten in spbc)
            {
                if (iten.SoLuongTonKho  <= chiTieu)
                    dssp.Add(iten);
            }
             return View(dssp);
        }



    }
}