﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
using PagedList;
using PagedList.Mvc;
using System.IO;
namespace DAweb.Controllers.Admin
{
   // [Authorize(Roles = "Admin")]
    public class AdminSPController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // GET: AdminSP
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DanhSachSanPham()
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            return View();
        }
        public  ActionResult DSSP()
            {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            return View(db.tblSanPhams.ToList());
            }
        // danh sách sản phẩm mới 
        public ActionResult DSSPMoi()
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            return View();
        }
        public ActionResult SanPhamAdminMoi(int? page)
        {   
            int pagenumber = (page ?? 1);
            int pageSize = 8;
            List<tblSanPham> ds = new List<tblSanPham>();
            var sp = db.tblSanPhams.ToList();
            foreach (var item in sp)
            {
                if (item.SPMoi == true)
                    ds.Add(item);
            }
            return View(ds.ToPagedList(pagenumber, pageSize));
        }

        public ActionResult SanPhamAdmin(int? page)
        {
            int pagenumber = (page ?? 1);
            int pageSize = 8;
            return View(db.tblSanPhams.ToList().OrderBy(n => n.MaSP).ToPagedList(pagenumber, pageSize));
        }
        //
        [HttpGet]
        public ActionResult ThemSanPham()
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }

            ViewBag.MaLoaiSP = new SelectList(db.tblLoaiSanPhams.ToList().OrderBy(n => n.TenLoaiSP), "MaLoaiSP", "TenLoaiSP");
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ThemSanPham(tblSanPham sp, HttpPostedFileBase fileUpload)
        {
            // đưa dữ liệu vào dropdownlist
            ViewBag.MaLoaiSP = new SelectList(db.tblLoaiSanPhams.ToList().OrderBy(n => n.TenLoaiSP), "MaLoaiSP", "TenLoaiSP");
            if (fileUpload == null)
            {
                ViewBag.ThongBao = "Chọn Hình Ảnh";
            }
             else  if(fileUpload!=null)
            {
                // thêm vào cơ sở dữ liệu 
                if (ModelState.IsValid)
                {     // lưu tên của file 
                    var fileName = Path.GetFileName(fileUpload.FileName);
                    // lưu đường dẫn của file 
                    var path = Path.Combine(Server.MapPath("~/App_Themes/HinhAnhSanPham"), fileName);
                    // kiểm tra hình ảnh đã tồn tại hay chưa 
                    if (System.IO.File.Exists(path))
                    {
                        ViewBag.ThongBao = " Hình ảnh Đã Tồn Tại";
                      
                    }
                    else
                    {
                        fileUpload.SaveAs(path);
                        sp.AnhMinhHoa = fileUpload.FileName;
                        db.tblSanPhams.Add(sp);
                        db.SaveChanges();
                        return RedirectToAction("DanhSachSanPham");
                    }
                }
            }
           
            return View();
        }
      //  chỉnh sửa sẩn phẩm
       [HttpGet]
        public ActionResult ChinhSua(int MaSP)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            // lấy ra đối tượng sách theo mã 
            tblSanPham sp = db.tblSanPhams.SingleOrDefault(n => n.MaSP == MaSP);
            if (sp == null)
            {
                Response.StatusCode = 404;
            }
            //Đưa dữ liệu vào dropdownlist
            ViewBag.MaLoaiSP = new SelectList(db.tblLoaiSanPhams.ToList().OrderBy(n => n.TenLoaiSP), "MaLoaiSP", "TenLoaiSP", sp.MaLoaiSP);
            return View(sp);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChinhSua(int MaSP, HttpPostedFileBase fileUpload)
        {
            var sp = db.tblSanPhams.SingleOrDefault(n => n.MaSP == MaSP);
            if (sp != null)
            {
            if (fileUpload != null)
            {
                    string hinh = sp.AnhMinhHoa;
                    string fullPath = Request.MapPath("~/App_Themes/HinhAnhSanPham/" + hinh);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                    // lưu tên của file 
                    var fileName = Path.GetFileName(fileUpload.FileName);
                // lưu đường dẫn của file 
                var path = Path.Combine(Server.MapPath("~/App_Themes/HinhAnhSanPham"), fileName);


                // kiểm tra hình ảnh đã tồn tại hay chưa 

                if (System.IO.File.Exists(path))
                {
                    ViewBag.ThongBao = " Hình ảnh Đã Tồn Tại";
                    //return View();
                }
                else
                {
                    fileUpload.SaveAs(path);
                    sp.AnhMinhHoa = fileUpload.FileName;
                }
            }
        }
            if (ModelState.IsValid)
            {
                //Thực hiện cập nhập trong model
                UpdateModel(sp);
                db.SaveChanges();
                ViewBag.ThongBao = " Cập Nhập Dữ Liệu Thanh Thành Công ";
                //Thêm vào cơ sở dữ liệu
                //Đưa dữ liệu vào dropdownlist
                ViewBag.MaLoaiSP = new SelectList(db.tblLoaiSanPhams.ToList().OrderBy(n => n.TenLoaiSP), "MaLoaiSP", "TenLoaiSP", sp.MaLoaiSP);
            }
            return RedirectToAction("DanhSachSanPham");

        }
        // hiên thí sản phẩm 
        //Hiển thị sản phẩm
        public ActionResult HienThi(int MaSP)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            //Lấy ra đối tượng sách theo mã 
            tblSanPham sp = db.tblSanPhams.SingleOrDefault(n => n.MaSP == MaSP);
            if (sp == null)
            {
                Response.StatusCode = 404;
                return null;
            }

            return View(sp);

        }
        //Xóa sản phẩm
        [HttpGet]
        public ActionResult Xoa(int MaSP)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            //Lấy ra đối tượng sách theo mã 
            tblSanPham sp = db.tblSanPhams.SingleOrDefault(n => n.MaSP == MaSP);
            if (sp == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(sp);
        }
        [HttpPost, ActionName("Xoa")]

        public ActionResult XacNhanXoa(int MaSP)
        {
            tblSanPham sp = db.tblSanPhams.SingleOrDefault(n => n.MaSP == MaSP);
            if (sp == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            string hinh = sp.AnhMinhHoa;
            string fullPath = Request.MapPath("~/App_Themes/HinhAnhSanPham/" + hinh);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
            db.tblSanPhams.Remove(sp);
            db.SaveChanges();
            return RedirectToAction("DanhSachSanPham");
        }
    }
}