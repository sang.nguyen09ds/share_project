﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;

namespace DAweb.Controllers.Admin
{
    public class LogonController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // GET: Logon
        [HttpGet]
        public ActionResult DangNhapAdmin()
        {
            return View();
        }
       
        [HttpPost]

        public ActionResult DangNhapAdmin(FormCollection f)
        {
            string sTaiKhoan = f["txtTaiKhoanAdmin"];
            string sMatKhau = MD5Hash(f.Get("txtMatKhau"));
            tblTaiKhoan Kh = db.tblTaiKhoans.SingleOrDefault(n => n.TenTaiKhoan == sTaiKhoan && n.MatKhau == sMatKhau && n.Role== "Admin");
            if (Kh != null)
            {  
                HttpCookie cookie = new HttpCookie("TaiKhoanAdmin");
                cookie["TenTaiKhoanAdmin"] = sTaiKhoan;
                cookie.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(cookie);
                ViewBag.ThongBao = " Chúc Mừng Bạn Đăng Nhập Thành Công ";
                Session["TaiKhoanAdmin"] = Kh;
              
                return RedirectToAction("Index", "HomeAdmin",Kh);
            }
            else
                ViewBag.ThongBao = " Tên Tài Khoản Hoặc Mật Khẩu Không đúng";
            return View();
        }
        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
        // thông tin tài khoản người Dùng 
        public ActionResult ThongTinTaiKhoanNguoiDung()
        {
            string TenTaiKhoan = Request.Cookies["TaiKhoanAdmin"]["TenTaiKhoanAdmin"];
            var tk = db.tblTaiKhoans.SingleOrDefault(n => n.TenTaiKhoan == TenTaiKhoan);
             if(tk==null)
            {
                Response.StatusCode = 404;
            }
               
            return View(tk);
        }
        // hồ sơ  cá nhân nhân người Dùng 
        public ActionResult HoSoCaNhanNguoiDung()
        {
            string TenTaiKhoan = Request.Cookies["TaiKhoanAdmin"]["TenTaiKhoanAdmin"];
            var tk = db.tblTaiKhoans.SingleOrDefault(n => n.TenTaiKhoan == TenTaiKhoan);
            if (tk == null)
            {
                Response.StatusCode = 404;
            }
            return View(tk);
        }
        // đăng xuất ra khỏi hệ thống
        public ActionResult logout()
        {
            if (Request.Cookies["TaiKhoanAdmin"] != null)
            {
                HttpCookie cookie = new HttpCookie("TaiKhoanAdmin");
                cookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(cookie);
            //    FormsAuthentication.SignOut();
            }
         //   FormsAuthentication.SignOut();
            return RedirectToAction("DangNhapAdmin", "Logon");
            
        }
    }
   
}