﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
using System.IO;

namespace DAweb.Controllers.Admin
{
    public class QuanLyDonHangController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // GET: QuanLyDonHang
        [HttpGet]
        public ActionResult DanhSachHoaDon()
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            return View(db.tblHoaDons.ToList());
        }
        [HttpGet]
        public ActionResult CTHD(int MaHD)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            List<tblCTHoaDon> cthd = db.tblCTHoaDons.Where(n=>n.MaHD==MaHD).ToList();
                 if(cthd==null)
                 {
                  Response.StatusCode = 404;
                 }
            ViewBag.IdDonHang = MaHD;
            return View(cthd);
        }
        [HttpPost]
        public ActionResult update(FormCollection f)
        {
            int MaHD = int.Parse(f["IDDonHang"]);
            tblHoaDon dh = db.tblHoaDons.First(m => m.MaHD == MaHD);
            if(dh==null)
            {
                Response.StatusCode = 404;
            }
            dh.TrangThai = "Đã xử Lý ";
            UpdateModel(dh);
            db.SaveChanges();
            return RedirectToAction("DanhSachHoaDon");
        }

        [HttpGet]
        public ActionResult ChinhSuaHoaDon(int MaHD)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            // lấy ra đối tượng sách theo mã 
            tblHoaDon HD = db.tblHoaDons.SingleOrDefault(n => n.MaHD == MaHD);
            if (HD == null)
            {
                Response.StatusCode = 404;
            }

            return View(HD);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChinhSuaHoaDon(tblHoaDon HD)
        {
            //Thêm vào cơ sở dữ liệu
            if (ModelState.IsValid)
            {
                //Thực hiện cập nhập trong model
                db.Entry(HD).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                ViewBag.ThongBao = "Cập Nhập Dữ Liệu Thanh Thành Công ";
            }

            //Đưa dữ liệu vào dropdownlist
            return RedirectToAction("DanhSachHoaDon");
        }

        public int XoaCTHD(int MaHD)
        {      
            //Lấy ra đối tượng sách theo mã 
            tblCTHoaDon sp = db.tblCTHoaDons.SingleOrDefault(n=>n.MaHD== MaHD);
            return sp.MaSP;
        }
        //Xóa sản phẩm
        [HttpGet]
        public ActionResult XoaHD(int MaHD)
        {
            if (Request.Cookies["TaiKhoanAdmin"] == null || Request.Cookies["TaiKhoanAdmin"].ToString() == "")
            {
                return RedirectToAction("DangNhapAdmin", "Logon");
            }
            //Lấy ra đối tượng sách theo mã 
            tblHoaDon sp = db.tblHoaDons.SingleOrDefault(n => n.MaHD == MaHD);
            if (sp == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(sp);
        }
        [HttpPost, ActionName("XoaHD")]

        public ActionResult XacNhanXoa(int MaHD)
        {
            tblHoaDon hd = db.tblHoaDons.SingleOrDefault(n => n.MaHD == MaHD);
            if (hd == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            List<tblCTHoaDon> cthd= db.tblCTHoaDons.Where(n => n.MaHD == MaHD).ToList();
                db.tblCTHoaDons.RemoveRange(cthd);
            db.tblHoaDons.Remove(hd);
            db.SaveChanges();
            return RedirectToAction("DanhSachHoaDon");
        }
    }
}