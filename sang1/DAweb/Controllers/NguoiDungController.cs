﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
using System.Web.Security;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
namespace DAweb.Controllers
{
    public class NguoiDungController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // GET: NguoiDung
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DangKy()
        {
            return View();
        }
        [HttpPost]  // lấy cơ dữ liệu theo phương thức post 
        [ValidateAntiForgeryToken]
        public ActionResult DangKy( FormCollection f , tblTaiKhoan tk )
        {        
                if (ModelState.IsValid)
                {
                string sTaiKhoan = f["TaiKhoan"];
                string Sktlengmk = f["Password"];
                string sPassword = MD5Hash(f["Password"]);              
                string sHoTen = f["HoTen"];
                string sSDT = f["SDT"];
                string sDiaChi = f["DiaChi"];
                string sEmail = f["Email"];
               tblTaiKhoan tkcsdl = db.tblTaiKhoans.SingleOrDefault(n => n.TenTaiKhoan == sTaiKhoan);
                tk.TenTaiKhoan = sTaiKhoan;
                tk.MatKhau = sPassword;
                tk.HoTen = sHoTen;
                tk.SDT = sSDT;
                tk.DiaChi = sDiaChi;
                tk.Email = sEmail;
                tk.Role = "User";
                if (tkcsdl == null)
                   {   if(tk.TenTaiKhoan.Length>=6)
                    {
                        if(Sktlengmk.Length >= 6)
                        {
                            // chèn dữ liệu vào bảng khách hàng 
                            db.tblTaiKhoans.Add(tk);
                            // lưu vào cơ sở dữ liệu 

                            ViewBag.ThongBao = " Đăng Kí Thàng Công";
                            db.SaveChanges();
                            return RedirectToAction("DangNhap", "NguoiDung");
                        }
                        else
                        {
                            ViewBag.ThongBao = "Độ Dài Mật Khẩu Qúa Ngắn ";
                        }
                    }
                   else
                    {
                        ViewBag.ThongBao = "Độ Dài Tài Khoản quá Ngắn ";
                    }
                   
                }
                else
                {
                    ViewBag.ThongBao = "Tài khoản đã tồn tại";
                }
            }
            return View();
        }
        // lấy theo phương thức get về chức năng đăng nhập 
        [HttpGet]
        public ActionResult DangNhap()
        {
            return View();
        }
        [HttpPost]
        public ActionResult DangNhap(FormCollection f)
        {
            string sTaiKhoan = f["TxtTaiKhoan"];
            string sMatKhau = MD5Hash(f.Get("txtMatKhau"));
            tblTaiKhoan Kh = db.tblTaiKhoans.SingleOrDefault(n => n.TenTaiKhoan == sTaiKhoan && n.MatKhau == sMatKhau);
            if (Kh != null)
            {   // thực hiện lưu cooike trong vòng 30 ngày 
                FormsAuthentication.SetAuthCookie(sTaiKhoan, false);
                var authTicket = new FormsAuthenticationTicket(1, sTaiKhoan, DateTime.Now, DateTime.Now.AddMinutes(20), false, Kh.Role);
                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                authCookie.Expires = DateTime.Now.AddDays(30);
                HttpContext.Response.Cookies.Add(authCookie);
                HttpCookie cookie = new HttpCookie("TaiKhoan");
                cookie["TenTaiKhoan"] = sTaiKhoan;
                cookie.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(cookie);
                ViewBag.ThongBao = " Chúc Mừng Bạn Đăng Nhập Thành Công ";
                ViewBag.DK = "1";
                Session["TaiKhoan"] = Kh;
                return RedirectToAction("Index","Home");
            }
            else 
            ViewBag.ThongBao = " Tên Tài Khoản Hoặc Mật Khẩu Không đúng";

          
            return View();
        }
        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
        // hàm có chức năng đăng xuất 
        public ActionResult Logout()
        {
            if (Request.Cookies["TaiKhoan"] != null)
            {
                HttpCookie cookie = new HttpCookie("TaiKhoan");
                cookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(cookie);
                FormsAuthentication.SignOut();
            }
            return RedirectToAction("Index", "Home");

        }
        public string GetUserCookie()
        {
            string cookieName = FormsAuthentication.FormsCookieName; //Find cookie name
            HttpCookie authCookie = HttpContext.Request.Cookies[cookieName]; //Get the cookie by it's name

            if (authCookie != null)
            {
                if (!string.IsNullOrEmpty(authCookie.Value))
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value); //Decrypt it
                    var UserName = ticket.Name; //You have the UserName!
                    if (UserName != null)
                    {
                        return UserName;
                    }
                }

            }

            return null;
        }
        // đang sai 
        // đổi mật khẩu
        [HttpGet]
        public ActionResult DoiMatKhau(string TenTaiKhoan)
        {
            tblTaiKhoan tk = db.tblTaiKhoans.SingleOrDefault(n => n.TenTaiKhoan == TenTaiKhoan);
            return View();
        }
        [HttpPost]
        public ActionResult DoiMatKhau (FormCollection f)
        {    string ktleng=(f["newPassword"]);
            string mkcu = MD5Hash(f["Password"]);
            string MkMoi = MD5Hash(f["newPassword"]);
            string NewMkMoi = MD5Hash(f["newconfirmPassword"]);
            string tenTK = Request.Cookies["TaiKhoan"]["TenTaiKhoan"].ToString();
            var tkkh = db.tblTaiKhoans.SingleOrDefault(n => n.TenTaiKhoan == tenTK);
            if (mkcu==tkkh.MatKhau)
            {      
                if(MkMoi==NewMkMoi)
                {
                    if(ktleng.Length>=6)
                    {
                        tkkh.MatKhau = MkMoi;
                        ViewBag.ThongBao = "Đổi Mật Khẩu Thành Công ";
                    }
                    else
                    {
                        ViewBag.ThongBao = "Độ Dài Mật Khẩu Quá Ngắn  ";
                        return View();
                    }
                   
                }
                else
                {
                    ViewBag.ThongBao = "Đổi Mật Khẩu Thất Bại ";
                    return View();
                }
            }
            else
            {
                ViewBag.ThongBao = "Đổi Mật Khẩu Thất Bại ";
                return View();
            }
            //Thêm vào cơ sở dữ liệu
            if (ModelState.IsValid)
            {
                //Thực hiện cập nhận trong model
                db.Entry(tkkh).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        // view user 
        public ActionResult ViewUserLink()
        {
            var userCookie = GetUserCookie();
            if (userCookie != null)
            {
                ViewBag.User = userCookie;
            }
           //   get User info
            return View();
        }
        /// Chỉnh Sửa Thông Tin Tài khoản cá nhân 
        [HttpGet]
        public ActionResult ThongTinTaiKhoanCaNhannd(string TenTaiKhoan)
        {    // lấy ra đối tượng sách theo mã 
            tblTaiKhoan tk = db.tblTaiKhoans.SingleOrDefault(n => n.TenTaiKhoan == TenTaiKhoan);
            if (tk == null)
            {
                Response.StatusCode = 404;
            }
            // thực hiện xóa hình cu đi upload hình mới vào 
            return View(tk);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ThongTinTaiKhoanCaNhannd(FormCollection f, HttpPostedFileBase fileUpload)
        {
            string sTaiKhoan = f["TenTaiKhoan"];
            string sEmail = f["Email"];
            string sSDT = f["SDT"];
            string sDiaChi = f["DiaChi"];
            string sHoTen = f["HoTen"];
            
            var tk = db.tblTaiKhoans.SingleOrDefault(n => n.TenTaiKhoan == sTaiKhoan);
            tk.Email = sEmail;
            tk.SDT = sSDT;
            tk.DiaChi = sDiaChi;
            tk.HoTen = sHoTen;
            if (tk == null)
            {
                Response.StatusCode = 404;
            }
            else
            {
                if (fileUpload != null)
                {
                    string hinh = tk.AnhDaiDien;
                    string fullPath = Request.MapPath("~/App_Themes/AnhDaiDienTK/" + hinh);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                    // lưu tên của file 
                    var fileName = Path.GetFileName(fileUpload.FileName);
                    // lưu đường dẫn của file 
                    var path = Path.Combine(Server.MapPath("~/App_Themes/AnhDaiDienTK"), fileName);
                    // kiểm tra hình ảnh đã tồn tại hay chưa 
                    if (System.IO.File.Exists(path))
                    {
                        ViewBag.ThongBao = " Hình ảnh Đã Tồn Tại";
                    }
                    else
                    {   // thực hiện xóa hình cu đi upload hình mới vào 
                        fileUpload.SaveAs(path);
                        tk.AnhDaiDien = fileUpload.FileName;
                    }
                }
            }
            //Thêm vào cơ sở dữ liệu
            if (ModelState.IsValid)
            {
                //Thực hiện cập nhận trong model
                db.Entry(tk).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("ThongTinTaiKhoanCaNhannd", new { @TenTaiKhoan =tk.TenTaiKhoan });
        }
        //[AllowAnonymous]
        [HttpGet]
        public ActionResult QuenMK()
        {
            return View();

        }
        [HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult QuenMK(string Email, FormCollection collection)
        {

            string ResertPasswordCodes = "";

            ResertPasswordCodes = CreateLostPassword(16);
            try
            {
                if (ModelState.IsValid)
                {
                    var senderEmail = new MailAddress("sangnguyen0909ds@gmail.com", "Nguyễn Văn Sang ");
                    var receiverEmail = new MailAddress(Email, "Receiver");
                    var password = "sangnguyen123";
                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(senderEmail.Address, password)

                    };

                    using (var message = new MailMessage(senderEmail, receiverEmail)
                    {

                        Subject = "Resert Password",
                        Body = ResertPasswordCodes,
                    })
                    {
                        smtp.Send(message);
                    }

                    tblTaiKhoan user = db.tblTaiKhoans.SingleOrDefault(n => n.Email == Email);
                    if (user != null)
                    {
                        user.ResertPasswordCode = ResertPasswordCodes;
                        db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                        //UpdateModel(user);
                        db.SaveChanges();
                    }
                    return RedirectToAction("ResertPassword", "NguoiDung", new { email = Email });
                }
            }
            catch (Exception)
            {
                ViewBag.Class = "label-danger";
                ViewBag.Error = "Lỗi";
            }
            ViewBag.Class = "label-danger";
            ViewBag.Error = "Email không tồn tại";
            return View();
        }
        [HttpGet]
        public ActionResult ResertPassword()
        {
            string email = Request.QueryString["Email"];
            ViewBag.email = email;
            return View();
        }
        [HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]

        public ActionResult ResertPassword(string Email, FormCollection collection)
        {

            var user = db.tblTaiKhoans.SingleOrDefault(n => n.Email == Email);
            string sPassword = collection["Password"];
            string sPasswordConfirm = collection["PasswordConfirm"];
            string sResertPassCodes = collection["ResertPassCodes"];
            ViewBag.email = Email;
            if (user != null)
            {
                if (user.ResertPasswordCode == sResertPassCodes)
                {
                    if (sPassword == sPasswordConfirm)
                    {
                       
                        if (user.MatKhau != sPassword)
                        {
                            user.MatKhau = MD5Hash(sPassword);
                            db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                            //UpdateModel(user);
                            db.SaveChanges();
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            ViewBag.Class = "label-danger";
                            ViewBag.ThongBao = " Mật khẩu mới không được trùng mật khẩu cũ !";
                            return View(new { email = user.Email });
                        }
                    }
                    else
                    {
                        ViewBag.Class = "label-danger";
                        ViewBag.ThongBao = " Mật khẩu không trùng nhau !";
                        return View(new { email = user.Email });
                    }
                }
                else
                {
                    ViewBag.Class = "label-danger";
                    ViewBag.ThongBao = " Mã không đúng !";
                    return View(new { email = user.Email });
                }

            }
            else
            {
                ViewBag.Class = "label-danger";
                ViewBag.ThongBao = " Email không tồn tại !";
                return View(new { email = user.Email });
            }



        }
        public string CreateLostPassword(int PasswordLenght)
        {
            string allowedChars = "abcdefghijkl0123456789mnopwrstuvwxyzABCDEFGHIJKLMNOPRSTUVWXYZ!@#$%^&*()_+=-.,';/][}{|";
            Random ranNum = new Random();
            char[] chars = new char[PasswordLenght];
            for (int i = 0; i < PasswordLenght; i++)
            {
                chars[i] = allowedChars[(int)((allowedChars.Length) * ranNum.NextDouble())];
            }
            return new string(chars);
        }

    }
}