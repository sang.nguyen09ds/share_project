﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
using PagedList;
using PagedList.Mvc;
namespace DAweb.Controllers
{
    public class SanPhamController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // GET: SanPham
        //public ActionResult SanPham()
        //{ var sp = db.tblSanPhams.ToList();
        //    return View(sp);
        //}
        // menu sản phẩm 
        public ActionResult DanhSachDanhMucCon(int MaDanhMuc)
        {
            var ds = db.tblLoaiSanPhams.Where(n => n.MaDanhMuc == MaDanhMuc).ToList();
            return View(ds);
        }
        // danh sách danh mục sản phẩm theo tiêu đề. sản phẩm
        public ActionResult Menu()
        {
            var DM = db.DanhMucs.ToList();
            return View(DM);
        }
        public ActionResult ListSanPhamDMCha(int MaDanhMuc)
        {
            //var lsp = db.tblLoaiSanPhams.ToList().Where(n => n.MaDanhMuc == MaDanhMuc);
            //    var dssp= db.tblSanPhams
            return View();
        }
        public ActionResult ListDMConTheoIDCha(int MaDanhMuc)
        {
            var DM = db.tblLoaiSanPhams.Where(n=>n.MaDanhMuc== MaDanhMuc).ToList();
            return View(DM);
        }
        public ActionResult DanhSachSanPham(int?page)
        {
            int pagenumber = (page ?? 1);
            int pageSize = 8;
            var sp = db.tblSanPhams.ToList().OrderBy(n => n.MaSP);
            return View(sp.ToPagedList(pagenumber, pageSize));
        }
        public ActionResult SanPhamTHeoLoai(int?page, int MaLoaiSP)
        {
            int pagenumber = (page ?? 1);
            int pageSize = 8;
            // kiểm tra xem loại sản phẩm có tồn tại hay không 
            tblLoaiSanPham LoaiSP = db.tblLoaiSanPhams.SingleOrDefault(n => n.MaLoaiSP == MaLoaiSP);
            if (LoaiSP == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            ViewBag.MaLoaiSP = LoaiSP.MaLoaiSP;
            // truy xuất các sản phẩm theo LoaiSP 
            List<tblSanPham> SP = db.tblSanPhams.Where(n => n.MaLoaiSP == MaLoaiSP).ToList();
            if (SP.Count == 0)
            {
                ViewBag.ThongBao = "Không có yêu cầu theo Loại Sản Phẩm này ";
            }
            return View(SP.ToPagedList(pagenumber, pageSize));
        }
        public ActionResult Size()
        {
            ViewBag.MaSize = new SelectList(db.SiZes.ToList().OrderBy(n => n.TenSize), "MaSize", "TenSize");
            return View();
        }
        public ActionResult SanPhamChiTiet(int? MaSP)
        {
            tblSanPham sanpham = db.tblSanPhams.SingleOrDefault(n => n.MaSP == MaSP);
              
            if (sanpham == null)
            {
                // trả về trang báo lỗi 
                Response.StatusCode = 404;
                return null;
            }
            db.SaveChanges();
            return View(sanpham);
        }
    }
}