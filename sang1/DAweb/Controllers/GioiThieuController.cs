﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
namespace DAweb.Controllers
{
    public class GioiThieuController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // GET: GioiThieu
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GioiThieu(int id=1)
        {
            var gt = db.GioiThieux.SingleOrDefault(n=>n.id==id);
            return View(gt);
        }
    }
}