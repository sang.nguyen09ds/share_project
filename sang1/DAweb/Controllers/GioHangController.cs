﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
namespace DAweb.Controllers
{
    public class GioHangController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // GET: GioHang

        public List<tblGioHangTao> LayGioHang()
        {
            List<tblGioHangTao> lstGioHang = Session["tblGioHangTao"] as List<tblGioHangTao>;
            if (lstGioHang == null)
            {
                // nếu giỏ hàng chưa tồn tại thì mình tiến hành  khởi tao list giỏ hàng (Session GioHang)
                lstGioHang = new List<tblGioHangTao>();
                Session["tblGioHangTao"] = lstGioHang;
            }
            return lstGioHang;
        }
      
      
        //// thêm giỏ hàng 
        public ActionResult ThemGioHang( int MASP, string strURL)
        {
            tblSanPham sp = db.tblSanPhams.SingleOrDefault(n => n.MaSP == MASP);
           
            if (sp == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            //Lấy ra session giỏ hàng
            List<tblGioHangTao> lstGioHang = LayGioHang();
            //Kiểm tra sách này đã tồn tại trong session[giohang] chưa
            tblGioHangTao sanpham = lstGioHang.Find(n => n.MaSP == MASP);
            if (sanpham == null)
            {
                sanpham = new tblGioHangTao(MASP);
                //Add sản phẩm mới thêm vào list
                lstGioHang.Add(sanpham);
                return Redirect(strURL);
            }
            else
            {
                sanpham.SoLuong++;
                return Redirect(strURL);
            }
        }
        // cập nhập giỏ hàng 
        public ActionResult CapNhapGioHang(int MASP, FormCollection f)
        {
            int soluong =int.Parse( f["txtSoLuong"].ToString());
            tblSanPham sach = db.tblSanPhams.SingleOrDefault(n => n.MaSP == MASP);
            if (sach == null)
            {
                Response.StatusCode = 404;
            }
            List<tblGioHangTao> lstGioHang = LayGioHang();
            tblGioHangTao sanpham = lstGioHang.SingleOrDefault(n => n.MaSP == MASP);
            
            if (sanpham != null)
            {   if(soluong<sach.SoLuongTonKho)
                {
                    sanpham.SoLuong = int.Parse(f["txtSoLuong"].ToString());
                    if(sanpham.SoLuong<1|| sanpham.SoLuong==0)
                    {
                        sanpham.SoLuong = 1;
                    }
                }
             else
                {
                    ViewBag.ThongBao = " Hàng Tạm Thời hết VUi lòng ĐỢi";
                }
                    
            }
            return RedirectToAction("GioHang");
        }
        // xóa giỏ hàng 
        public ActionResult XoaGioHang(int MASP)
        {
            //Kiểm tra masp
            tblSanPham sach = db.tblSanPhams.SingleOrDefault(n => n.MaSP == MASP);
            //Nếu get sai masp thì sẽ trả về trang lỗi 404
            if (sach == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            //Lấy giỏ hàng ra từ session
            List<tblGioHangTao> lstGioHang = LayGioHang();
            tblGioHangTao sanpham = lstGioHang.SingleOrDefault(n => n.MaSP == MASP);
            //Nếu mà tồn tại thì chúng ta cho sửa số lượng
            if (sanpham != null)
            {
                lstGioHang.RemoveAll(n => n.MaSP == MASP);
             
            }
            if (lstGioHang.Count == 0)
            {
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("GioHang");
        }
        // xây dựng trang giỏ hàng 
        public ActionResult GioHang()
        {
            if (Session["tblGioHangTao"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            List<tblGioHangTao> lstGioHang = LayGioHang();
            return View(lstGioHang);
        }
        //Tính tổng số lượng và tổng tiền
        public int TongSoLuong()
        {
            int iTongSoLuong = 0;
            List<tblGioHangTao> lstGioHang = Session["tblGioHangTao"] as List<tblGioHangTao>;
            if (lstGioHang != null)
            {
                iTongSoLuong = lstGioHang.Sum(n => n.SoLuong);
            }
            return iTongSoLuong;
        }
        //Tính tổng thành tiền
        public double TongTien()
        {
            double dTongTien = 0;
            List<tblGioHangTao> lstGioHang = Session["tblGioHangTao"] as List<tblGioHangTao>;
            if (lstGioHang != null)
            {
                dTongTien = lstGioHang.Sum(n => n.dThanhTien);
            }
            return dTongTien;
        }
        //  tạo ra partial giỏ hàng
        public ActionResult GioHangPartial()
        {
            if (TongSoLuong() == 0)
            {
                return PartialView();
            }
            ViewBag.TongSoLuong = TongSoLuong();
            ViewBag.TongTien = TongTien();
            return PartialView();
        }
        // xây dựng 1 View cho người dùng chinh sửa giỏ hàng 
        public ActionResult SuaGioHang()
        {
            if (Session["tblGioHangTao"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            List<tblGioHangTao> lstGioHang = LayGioHang();
            return View(lstGioHang);
        }
    }

    
}