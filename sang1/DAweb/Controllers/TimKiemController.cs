﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
using PagedList.Mvc;
using PagedList;
namespace DAweb.Controllers
{
    public class TimKiemController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // GET: TimKiem
        // chuyển từ số sang chuỗi
        [HttpPost]
        public ActionResult KetQuaTimKiem(FormCollection f, int? page)
        {       // kiểm tra coi chuỗi nhập vào là chuỗi hay kiểu dữ liệu số
            double price;
             bool isDouble = Double.TryParse(f["txtTimKiem"], out price);
                 if (!isDouble)         
              {
                string sTuKhoa = f["txtTimKiem"].ToString();
                ViewBag.TuKhoa = sTuKhoa;
                List<tblSanPham> lsKqTK = db.tblSanPhams.Where(n => n.TenSP.Contains(sTuKhoa)).ToList();
                // phân trang 
                int panumber = (page ?? 1);
                int pagesize = 8;
                if (lsKqTK.Count == 0)
                {
                    ViewBag.ThongBao = "Không Tìm Thấy Sản Phẩm Nào ";
                    //return View(db.tblSanPhams.OrderBy(n => n.TenSP).ToPagedList(panumber, pagesize));
                    return View(lsKqTK.OrderBy(n => n.TenSP).ToPagedList(panumber, pagesize));
                }
                ViewBag.ThongBao = "Đã tìm thấy " + lsKqTK.Count + " kết quả!";
                return View(lsKqTK.OrderBy(n => n.TenSP).ToPagedList(panumber, pagesize));

            }
            else
            {
                  double sTuKhoa = double.Parse(f["txtTimKiem"].ToString());
                ViewBag.TuKhoa = sTuKhoa;
                //  List<tblSanPham> lsKqTK = db.tblSanPhams.Where(n => n.TenSP.Contains(sTuKhoa)).ToList();
                //   double vtien = dGiaTien;
                List<tblSanPham> lstKQTK = db.tblSanPhams.ToList();
                List<tblSanPham> dssp = new List<tblSanPham>();
                foreach (var item in lstKQTK)
                {
                    if (item.GiaTien <= sTuKhoa)
                    {
                        dssp.Add(item);
                    }
                }
                // phân trang 
                int panumber = (page ?? 1);
                int pagesize = 8;
                if (dssp.Count == 0)
                {
                    ViewBag.ThongBao = "Không Tìm Thấy Sản Phẩm Nào ";
                    //return View(db.tblSanPhams.OrderBy(n => n.TenSP).ToPagedList(panumber, pagesize));
                    return View(dssp.OrderBy(n => n.GiaTien).ToPagedList(panumber, pagesize));
                }
                ViewBag.ThongBao = "Đã tìm thấy " + dssp.Count + " kết quả!";
                return View(dssp.OrderBy(n => n.GiaTien).ToPagedList(panumber, pagesize));
            }
        }

    
        [HttpGet]
        public ActionResult KetQuaTimKiem(int? page, string sTuKhoa)
        {
            ViewBag.TuKhoa = sTuKhoa;
            double price;
            bool isDouble = Double.TryParse(sTuKhoa, out price);
            if (!isDouble)
            {
               
                List<tblSanPham> lstKQTK = db.tblSanPhams.Where(n => n.TenSP.Contains(sTuKhoa)).ToList();
                //Phân trang
                int pageNumber = (page ?? 1);
                int pageSize = 8;
                if (lstKQTK.Count == 0)
                {
                    ViewBag.ThongBao = "Không tìm thấy sản phẩm nào ";
                    //  return View(db.tblSanPhams.OrderBy(n => n.TenSP).ToPagedList(pageNumber, pageSize));
                    return View(lstKQTK.OrderBy(n => n.TenSP).ToPagedList(pageNumber, pageSize));
                }
                ViewBag.ThongBao = "Đã tìm thấy " + lstKQTK.Count + " kết quả!";
                return View(lstKQTK.OrderBy(n => n.TenSP).ToPagedList(pageNumber, pageSize));
            }
            else
            {
               // ViewBag.TuKhoa = sTuKhoa;
                double vtien = double.Parse(sTuKhoa);
                List<tblSanPham> lstKQTK = db.tblSanPhams.ToList();
                List<tblSanPham> dssp = new List<tblSanPham>();
                foreach (var item in lstKQTK)
                {
                    if (item.GiaTien <= vtien)
                    {
                        dssp.Add(item);
                    }
                }
                //Phân trang
                int pageNumber = (page ?? 1);
                int pageSize = 8;
                if (dssp.Count == 0)
                {
                    ViewBag.ThongBao = "Không tìm thấy sản phẩm nào ";
                    //  return View(db.tblSanPhams.OrderBy(n => n.TenSP).ToPagedList(pageNumber, pageSize));
                    return View(dssp.OrderBy(n => n.GiaTien).ToPagedList(pageNumber, pageSize));
                }
                ViewBag.ThongBao = "Đã tìm thấy " + dssp.Count + " kết quả!";
                return View(dssp.OrderBy(n => n.GiaTien).ToPagedList(pageNumber, pageSize));
            }
        }
    }
}