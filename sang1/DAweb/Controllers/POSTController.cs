﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAweb.Models;
using PagedList;
using PagedList.Mvc;
namespace DAweb.Controllers
{
    public class POSTController : Controller
    {
        dbWebBanHangEntities db = new dbWebBanHangEntities();
        // GET: POST
        public ActionResult Post(int ID)
        {
            List<POST> post = db.POSTs.Where(x => x.id == ID).ToList();
            return PartialView("Post", post);
        }
        public ActionResult DanhSachPost(int? page)
        {
            var model = db.POSTs;
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(model.ToList().OrderBy(n => n.id).ToPagedList(pageNumber, pageSize));
        }
        public ActionResult DanhMucPost()
        {
            List<POST> post = db.POSTs.ToList();
            return PartialView("DanhMucPost", post);
        }
        public ActionResult DanhSachPostAdmin()
        {

            return View();
        }
        public ActionResult ViewPost()
        {
            List<POST> post = db.POSTs.ToList();
            return PartialView("ViewPost", post);
        }
    }
}