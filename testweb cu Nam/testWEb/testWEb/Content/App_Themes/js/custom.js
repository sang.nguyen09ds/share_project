jQuery(function($) {
    
	
	$('#slider').revolution({
	                                             
    	delay:9000,
		startwidth:1170,
		startheight:550,
		hideThumbs:10

    });	
	// slider product
	/*$(".wrap-cat-product").each(function(){
		$(this).find(".wrap-list-product").carouFredSel({
			    auto:true,
				circular: true,
				infinite: true,
				scroll:{duration: 800, items: 1},
				
				prev : $(this).find(".cat-prev"),
				next : $(this).find(".cat-next"),
		});
	});*/
   
	// Click Textbox and Area
	
	$(".TextBoxField,textarea")
	  .focus(function() {
			if (this.value === this.defaultValue) {
				this.value = '';
			}
	  })
	  .blur(function() {
			if (this.value === '') {
				this.value = this.defaultValue;
			}
	});
	
		
		$("#menu .btn-menu").click(function(){

    			$(this).toggleClass('tooglemn').siblings('.nav-parent').slideToggle();

    	});
		
		
		
	
	 // Using custom configuration
	 
	 	// back to top
	jQuery(window).scroll(function() {
    if (jQuery(this).scrollTop()) {
        jQuery('.btn-back-top').fadeIn();
    } else {
        jQuery('.btn-back-top').fadeOut();
    }
	});

	jQuery(".btn-back-top").click(function() {
		jQuery("html, body").animate({scrollTop: 0}, 1000);
	 });
	 

	
	var win_w=$(window).width();
		
		if(win_w<=780)
		{
			$(".sidebar-right .title-block").click(function(){

    			$(this).siblings('.content').slideToggle();

    		});
			
			 $(".link-footer h3").click(function(){

    			$(this).siblings('.content-col').slideToggle().parents('.col-2').siblings().children('.content-col').hide('slow');

    		});
		
		}
		
		
		
		$("#menu-footer .btn-menu").click(function(){

    			$(this).siblings('.menu-footer').slideToggle();

    	});
		
		
		
		$(".list-category > li > a").click(function(e) {
		
		
		if($(this).siblings("ul").size() > 0)
		{
			
			$(this).toggleClass("active");
			
			$(this).siblings("ul").slideToggle();
			
			return false;
		}
		
    });
	
	
	$(".order .button .up").click(function(e) {
        var val = $(".order .TextBoxField").val();
		$(".order .TextBoxField").val(parseInt(val)+1);
    });
	
	$(".order .button .down").click(function(e) {
        var val = $(".order .TextBoxField").val();
		 if(val > 1){
			 $(".order .TextBoxField").val(parseInt(val)-1);
		 }
    });
	  
	
	
});

$(window).load(function(){

		
		$(".flex-customer ul.listcustomer").carouFredSel({
			auto:false,
			width : "100%", 
			height: 'variable',
			responsive:true,
			circular: true,
			infinite: true,
			scroll:{duration: 600, items: 1},
			items: {
						items: 1,
						fx : "fade",
						easing : "linear",
						duration : 1000
			},
			pagination: ".pager-flex",
			
		});
		
		
		/*$(".wrap-slider-thum ul").carouFredSel({
				auto:true,
				height: 'variable',
				responsive:true,
				circular: true,
				infinite: true,
				scroll:{duration: 800, items: 1},
				items: {
						    width : 78, 
							height: 'variable',
							visible: {
								min: 1,
								max: 3
							}
				},
				prev : ".prev-sl-thum",
				next : ".next-sl-thum",
				
			});
		*/
		
		
		$(".slider-sp ul").carouFredSel({
				auto:true,
				width : "100%", 
				height: 'variable',
				responsive:true,
				circular: true,
				infinite: true,
				scroll:{duration: 800, items: 1},
				items: { 
							height: 'variable',
				},
				prev : ".btn-control-slider.btn-sl-left",
				next : ".btn-control-slider.btn-sl-right",
				
			});
	

	      $(".row-doitackh ul").carouFredSel({
				auto:true,
				width : "100%", 
				height: 'variable',
				responsive:true,
				circular: true,
				infinite: true,
				scroll:{duration: 800, items: 1},
				items: {
						    width : 170, 
							height: 'variable',
							visible: {
								min: 1,
								max: 6
							}
				},
				prev : ".wrap-control-cat-doitac .cat-prev",
				next : ".wrap-control-cat-doitac .cat-next",
			});
			
			$(".list-slide-relative-product").carouFredSel({
			auto:true,
			width : "100%", 
			height: 'variable',
			responsive:true,
			circular: true,
			infinite: true,
			scroll:{duration: 600, items: 1},
			items: {
						width: 168,
						height: 'variable',
						visible: {
							min: 1,
							max: 5
						}
			},
			fx:'fade',
			prev : ".prev1",
    		next : ".next1"
			
		});
		
		
		/* slider product
		$(".page .wrap-cat-product").each(function(){
			$(this).find(".list-product").carouFredSel({
				width: "100%",
				height:'variable',
				auto:false,
				circular: true,
				infinite: true,
				items: {
						width: 270,
						height: 'variable',
						visible: {
							min: 1,
							max: 4
						}
			    },
				direction: "left",
				scroll:{duration: 500, items: 1},
				items: 4,
				prev: $(this).find(".cat-prev"),
				next: $(this).find(".cat-next")
			});
		});*/
			
			
});

$(window).load(function(){
	
	
	$('.tabs .tab-wrap').hide();
	$('.tabs .tab-wrap:first').show();
	$('.tabs .ul-tabs li:first').addClass('active');
	 
	$('.tabs .ul-tabs li a').click(function(){
	$('.tabs .ul-tabs li').removeClass('active');
	$(this).parent().addClass('active');
	var currentTab = $(this).attr('href');
	$('.tabs .tab-wrap').hide();
	$(currentTab).show();
	return false;
	});
	
});

