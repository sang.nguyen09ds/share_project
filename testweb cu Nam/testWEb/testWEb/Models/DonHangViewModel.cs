﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace testWEb.Models
{
    public class DonHangViewModel
    {
        public long ID { get; set; }
        public long IDDH { get; set; }
        public string NgayTao { get; set; }
        public long IDSP { get; set; }
        public long IDKH { get; set; }
        public string TenSP { get; set; }
        public string HoKH { get; set; }
        public string TenKH { get; set; }
        public string Email { get; set; }
        public int SoLuong { get; set; }
        public int DonGia { get; set; }
        public int? ThanhTien { get; set; }
        public int? TinhTrang { get; set; }
    }
}