﻿using System.Web;
namespace testWEb.Models
{
    using System;
    using System.Collections.Generic;

    public partial class Image
    {
        public long ID { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public Nullable<long> ProductID { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }
    }
}