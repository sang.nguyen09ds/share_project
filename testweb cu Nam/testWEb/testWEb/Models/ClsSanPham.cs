﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
namespace testWEb.Models
{
    public class ClsSanPham
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public float Price { get; set; }
        public float Sale_Price { get; set; }
        public int Quantity { get; set; }
        public string Short_Description { get; set; }
        public string Description { get; set; }
        public int Categories { get; set; }
        public HttpPostedFileBase Anh { get; set; }
        public string Product_tag { get; set; }


    }

}