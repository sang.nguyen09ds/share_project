﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace testWEb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "TrangChu",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "TrangChu", action = "Index", id = UrlParameter.Optional }
           );
            routes.MapRoute(
               name: "Admin",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "HomeAdmin", action = "Index", id = UrlParameter.Optional }
           );
            routes.MapRoute(
                name: "ThongTinTaiKhoanCaNhan",
                url: "{controller}/{action}",
                defaults: new { controller = "User", action = "ThongTinTaiKhoanCaNhan"}
                );
        }
    }
}
