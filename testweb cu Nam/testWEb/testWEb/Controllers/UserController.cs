﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using testWEb.Models;
namespace testWEb.Controllers
{

    public class UserController : Controller
    {
        QLBHEntities db = new QLBHEntities();

        public static string TaiKhoan { get; set; }


        // GET: User
        [HttpGet]
        public ActionResult DangKy()
        {
            return View();
        }
        [HttpPost]  // lấy cơ dữ liệu theo phương thức post 

        [ValidateInput(false)]

        public ActionResult DangKy(FormCollection collection, USER tk)
        {
            if (ModelState.IsValid)
            {
                string sTaiKhoan = collection["TaiKhoan"];
                string sPassword = MD5Hash(collection["Password"]);
                string sHoTen = collection["HoTen"];
                string sSDT = collection["SDT"];
                string sDiaChi = collection["DiaChi"];
                string sEmail = collection["Email"];

                USER tkcsdl = db.USERs.SingleOrDefault(n => n.TaiKhoan == sTaiKhoan);


                if (tkcsdl == null)
                {  // chèn dữ liệu vào bảng khách hàng 
                    tk.TaiKhoan = sTaiKhoan;
                    tk.Password = sPassword;
                    tk.HoTen = sHoTen;
                    tk.SDT = sSDT;
                    tk.Email = sEmail;
                    tk.Role = "User";
                    db.USERs.Add(tk);
                    // lưu vào cơ sở dữ liệu 
                    ViewBag.ThongBao = "Đăng Ký Thàng Công";
                    ViewBag.Class = "label-success";
                    db.SaveChanges();
                    return RedirectToAction("Index", "TrangChu");
                }
                else
                {
                    ViewBag.ThongBao = "Tài khoản đã tồn tại";
                    ViewBag.Class = "label-danger";
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult DangNhap()
        {
            return View();
        }
        [HttpPost]
        public ActionResult DangNhap(FormCollection collection)
        {

            if (!ModelState.IsValid)
            {
                return View();
            }

            string sTaiKhoan = collection["TaiKhoan"];
            string sPassword = MD5Hash(collection["Password"]);
            USER user = db.USERs.SingleOrDefault(n => n.TaiKhoan == sTaiKhoan && n.Password == sPassword);
            if (user != null)
            {

                FormsAuthentication.SetAuthCookie(sTaiKhoan, false);
                var authTicket = new FormsAuthenticationTicket(1, sTaiKhoan, DateTime.Now, DateTime.Now.AddMinutes(20), false, user.Role);
                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                authCookie.Expires = DateTime.Now.AddDays(30);
                HttpContext.Response.Cookies.Add(authCookie);


                if (user.Role == "Admin")
                {
                    return RedirectToAction("Index", "HomeAdmin", user);
                }
                return RedirectToAction("Index", "TrangChu", user);

            }
            else
            {
                ModelState.AddModelError("", "Invalid login attempt.");
                ViewBag.Class = "label-danger";
                ViewBag.ThongBao = " Tên Tài Khoản Hoặc Mật Khẩu Không đúng";
                return View();
            }


        }



        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "TrangChu");

        }

        public string GetUserCookie()
        {
            string cookieName = FormsAuthentication.FormsCookieName; //Find cookie name
            HttpCookie authCookie = HttpContext.Request.Cookies[cookieName]; //Get the cookie by it's name

            if (authCookie != null)
            {
                if (!string.IsNullOrEmpty(authCookie.Value))
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value); //Decrypt it
                    var UserName = ticket.Name; //You have the UserName!
                    if (UserName != null)
                    {
                        return UserName;
                    }
                }

            }

            return null;
        }
        public ActionResult ViewUserLink()
        {
            var userCookie = GetUserCookie();
            if (userCookie != null)
            {
                ViewBag.User = userCookie;
            }
            // get User info
            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult QuanLyTKAdmin()
        {
            var model = db.USERs;
            return PartialView("QuanLyTKAdmin", model);
        }


        public ActionResult Xoa(int ID)
        {
            USER pr = db.USERs.SingleOrDefault(n => n.UserID == ID);
            if (pr == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            db.USERs.Remove(pr);
            db.SaveChanges();

            return RedirectToAction("QuanLyTKAdmin", "User");

            // xoa trong table product ,
            // time kiem hinh anh co id san pham do trong table image va xoa no

        }

        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        public ActionResult Edit(int ID)
        {
            USER user = db.USERs.SingleOrDefault(n => n.UserID == ID);
            return PartialView(user);
        }
        [HttpPost]
        public ActionResult Edit(int ID, FormCollection collection)
        {
            USER user = db.USERs.SingleOrDefault(m => m.UserID == ID);
            string sRole = collection["Role"];

            user.Role = sRole;
            //UpdateModel(user);
            db.Entry(user).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("QuanLyTKAdmin", "User");
        }
        public ActionResult ThongTinTaiKhoanCaNhan(string username)
        {

            var model = db.USERs.Where(m => m.TaiKhoan == username);
            return PartialView(model);
        }
        [HttpGet]
        public ActionResult SuaThongTinCaNhan(int ID)
        {
            var model = db.USERs.First(m => m.UserID == ID);
            return PartialView(model);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SuaThongTinCaNhan(int ID, FormCollection collection)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var user = db.USERs.First(m => m.UserID == ID);
            string sPassword = MD5Hash(collection["Password"]);


            if (user != null)
            {
                user.Password = sPassword;
                db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                //UpdateModel(user);
                db.SaveChanges();
                return RedirectToAction("ThongTinTaiKhoanCaNhan", "User", new { username = user.TaiKhoan });
            }
            else
            {
                ModelState.AddModelError("", "Invalid login attempt.");
                ViewBag.Class = "label-danger";
                ViewBag.ThongBao = " Lỗi";
                return View();
            }
        }
        [HttpGet]
        public ActionResult DoiMatKhau()
        {
            
            return View();
        }
        [HttpPost]
        public ActionResult DoiMatKhau(int ID, FormCollection collection)
        {

            var user = db.USERs.SingleOrDefault(n => n.UserID == ID);
            string sOldPassword = MD5Hash(collection["OldPassword"]);
            string sPassword = MD5Hash(collection["Password"]);
            string sPasswordConfirm = MD5Hash(collection["PasswordConfirm"]);

            if (user != null)
            {
                if (user.Password == sOldPassword)
                {
                    if (sOldPassword != sPassword)
                    {
                        if (sPassword == sPasswordConfirm)
                        {
                            user.Password = MD5Hash(sPassword);
                            db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                            //UpdateModel(user);
                            db.SaveChanges();

                            return RedirectToAction("ThongTinTaiKhoanCaNhan", "User", new { username = user.TaiKhoan });


                        }
                        else
                        {
                            ViewBag.Class = "label-danger";
                            ViewBag.ThongBao = " Mật khẩu không trùng nhau !";
                            return View(new { ID = user.UserID });
                        }

                    }
                    else
                    {
                        ViewBag.Class = "label-danger";
                        ViewBag.ThongBao = " Mật khẩu không được trùng mật khẩu cũ !";
                        return View(new { ID = user.UserID });
                    }
                }
                else
                {
                   
                        ViewBag.Class = "label-danger";
                        ViewBag.ThongBao = " Mật khẩu không đúng !";
                        return View(new { ID = user.UserID });
                   
                }
            }
            else
            {
                ViewBag.Class = "label-danger";
                ViewBag.ThongBao = " Tài khoảng không tồn tại !";
                return View();
            }

        }


        //[AllowAnonymous]
        [HttpGet]
        public ActionResult QuenMK()
        {
            return View();

        }
        [HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public /*async Task<*/ActionResult/*>*/ QuenMK(string Email, FormCollection collection)
        {

            string ResertPasswordCodes = "";

            ResertPasswordCodes = CreateLostPassword(16);
            try
            {
                if (ModelState.IsValid)
                {
                    var senderEmail = new MailAddress("my.love.445.for.u@gmail.com", "Nguyen Thanh Nam");
                    var receiverEmail = new MailAddress(Email, "Receiver");
                    var password = "@Nhatnam123";
                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(senderEmail.Address, password)

                    };

                    using (var message = new MailMessage(senderEmail, receiverEmail)
                    {

                        Subject = "Resert Password",
                        Body = ResertPasswordCodes,
                    })
                    {
                        smtp.Send(message);
                    }

                    USER user = db.USERs.SingleOrDefault(n => n.Email == Email);
                    if (user != null)
                    {
                        user.ResertPasswordCode = ResertPasswordCodes;
                        db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                        //UpdateModel(user);
                        db.SaveChanges();
                    }
                    return RedirectToAction("ResertPassword", "User", new { email = Email });
                }
            }
            catch (Exception)
            {
                ViewBag.Class = "label-danger";
                ViewBag.Error = "Lỗi";
            }
            ViewBag.Class = "label-danger";
            ViewBag.Error = "Email không tồn tại";
            return View();
        }
        [HttpGet]
        public ActionResult ResertPassword()
        {
            string email = Request.QueryString["email"];
            ViewBag.email = email;
            return View();

        }
        [HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]

        public ActionResult ResertPassword(string email, FormCollection collection)
        {

            var user = db.USERs.SingleOrDefault(n => n.Email == email);
            string sPassword = collection["Password"];
            string sPasswordConfirm = collection["PasswordConfirm"];
            string sResertPassCodes = collection["ResertPassCodes"];
            ViewBag.email = email;
            if (user != null)
            {
                if (user.ResertPasswordCode == sResertPassCodes)
                {
                    if (sPassword == sPasswordConfirm)
                    {
                        if (user.Password == sPassword)
                        {
                            user.Password = MD5Hash(sPassword);
                            db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                            //UpdateModel(user);
                            db.SaveChanges();
                            return RedirectToAction("Index", "TrangChu");
                        }
                        else
                        {
                            ViewBag.Class = "label-danger";
                            ViewBag.ThongBao = " Mật khẩu mới không được trùng mật khẩu cũ !";
                            return View(new { email = user.Email });
                        }
                    }
                    else
                    {
                        ViewBag.Class = "label-danger";
                        ViewBag.ThongBao = " Mật khẩu không trùng nhau !";
                        return View(new { email = user.Email });
                    }
                }
                else
                {
                    ViewBag.Class = "label-danger";
                    ViewBag.ThongBao = " Mã không đúng !";
                    return View(new { email = user.Email });
                }

            }
            else
            {
                ViewBag.Class = "label-danger";
                ViewBag.ThongBao = " Email không tồn tại !";
                return View(new { email = user.Email });
            }



        }
        public string CreateLostPassword(int PasswordLenght)
        {
            string allowedChars = "abcdefghijkl0123456789mnopwrstuvwxyzABCDEFGHIJKLMNOPRSTUVWXYZ!@#$%^&*()_+=-.,';/][}{|";
            Random ranNum = new Random();
            char[] chars = new char[PasswordLenght];
            for (int i = 0; i < PasswordLenght; i++)
            {
                chars[i] = allowedChars[(int)((allowedChars.Length) * ranNum.NextDouble())];
            }
            return new string(chars);
        }

    }

}

