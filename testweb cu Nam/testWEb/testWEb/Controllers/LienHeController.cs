﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace testWEb.Controllers
{
    public class LienHeController : Controller
    { 
        // GET: LienHe
        
        public ActionResult LienHe()
        {

            return View();
        }
        [HttpPost]
        public ActionResult LienHe(string TieuDe, string Email,string NoiDung)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    var senderEmail = new MailAddress("my.love.445.for.u@gmail.com", "Nguyen Thanh Nam");
                    var receiverEmail = new MailAddress(Email, "Receiver");
                    var password = "@Nhatnam123";
                    var sub = TieuDe;
                    var body = NoiDung;
                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(senderEmail.Address, password)

                    };
                    using (var message = new MailMessage(senderEmail, receiverEmail)
                    {
                        Subject = TieuDe,
                        Body = NoiDung

                    })
                    {
                        smtp.Send(message);
                    }
                    return View();
                }
            }
            catch(Exception)
            {
                ViewBag.Error = "Lỗi";
            }

            return View();
        }
    }
}