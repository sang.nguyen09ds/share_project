﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testWEb.Models;
using PagedList;
using PagedList.Mvc;

namespace testWEb.Controllers.Admin
{
  
    [Authorize(Roles = "Admin")]
    public class PostAdminController : Controller
    {
       
        QLBHEntities db = new QLBHEntities();
        // GET: PostAdmin
        [HttpGet]
        public ActionResult CreatePost()
        {
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreatePost(FormCollection collection,ImagePost img,POST pOST)
        {
            string sPostName = collection["PostName"];
            string sMotaNgan = collection["MoTaNgan"];
            string sMoTa = collection["MoTaBV"];
          
          
            pOST.PostName = sPostName;
            pOST.Description = sMoTa;
            pOST.Short_Description = sMotaNgan;

            string fileName = Path.GetFileNameWithoutExtension(img.Thumbnail.FileName);
            string extention = Path.GetExtension(img.Thumbnail.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extention;
            img.ImagePath = "~/Images/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Images/"), fileName);
            img.Thumbnail.SaveAs(fileName);
            if (fileName != null)
            {
                pOST.Thumbnail = img.ImagePath;
            }

            db.POSTs.Add(pOST);
            db.SaveChanges();
            return View(collection);


        }
        public ActionResult PostAdmin(int? page)
        {
            var model = db.POSTs;
            int pageSize = 9;
            int pageNumber = (page ?? 1);
            return PartialView("PostAdmin",model.ToList().OrderBy(n => n.Id).ToPagedList(pageNumber, pageSize));
        }
        public ActionResult DanhSachPostAdmin()
        {
            
            
            return View();
        }
        private QLBHEntities context = null;

        public ActionResult Edit(int PostID)
        {
            var pr = db.POSTs.First(m => m.Id == PostID);
            return View(pr);
        }
        [HttpPost]
        public ActionResult Edit(int PostID, FormCollection collection, POST post, Image1 img)
        {
            var pr = db.POSTs.First(m => m.Id == PostID);

            string sPostName = collection["PostName"];
            string sMotaNgan = collection["MoTaNgan"];
            string sMoTa = collection["MoTaBV"];

            if (pr.Thumbnail != img.ImagePath)
            {
                string fileName = Path.GetFileNameWithoutExtension(img.ImageFile.FileName);
                string extention = Path.GetExtension(img.ImageFile.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extention;
                img.ImagePath = "~/Images/" + fileName;
                fileName = Path.Combine(Server.MapPath("~/Images/"), fileName);
                img.ImageFile.SaveAs(fileName);
                pr.Thumbnail = img.ImagePath;
            }


            post.PostName = sPostName;
            post.Description = sMoTa;
            post.Short_Description = sMotaNgan;

            // image

            UpdateModel(pr);

            db.SaveChanges();
            return RedirectToAction("DanhSachPostAdmin", "PostAdmin");
        }
        public ActionResult Xoa(int PostID)
        {
            POST pr = db.POSTs.SingleOrDefault(n => n.Id == PostID);
            if (pr == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            db.POSTs.Remove(pr);
            db.SaveChanges();

            return RedirectToAction("DanhSachPostAdmin", "PostAdmin");

            // xoa trong table product ,
            // time kiem hinh anh co id san pham do trong table image va xoa no

        }
    }
}