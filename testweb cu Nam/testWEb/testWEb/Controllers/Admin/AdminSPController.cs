﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testWEb.Models;
using PagedList;
using PagedList.Mvc;
namespace testWEb.Views.Admin
{
   
    [Authorize(Roles = "Admin")]
    public class AdminSPController : Controller
    {
        QLBHEntities db = new QLBHEntities();
        // GET: ProductAdmin

        [HttpGet]
        public ActionResult ThemSanPham()
        {
            return View();
        }
      

       
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ThemSanPham(FormCollection collection, PRODUCT pr, Image1 img,PR_CATEGORIES cat, CategoryModel catmodel)
        {
            string sTenSP = collection["TenSP"];
            int sDonGia = int.Parse(collection["Gia"]);
            int sGiasale = int.Parse(collection["GiaMoi"]);
            string sMotaNgan = collection["MoTaNgan"];
            string sMoTa = collection["MoTaSP"];
            string sTag = collection["TuKhoa"];
            string sDM = collection["TenDM"];
            int sSoLuong = int.Parse(collection["SoLuong"]);

            pr.ProductName = sTenSP;
            pr.Price = sDonGia;
            pr.Sale_Price = sGiasale;
            pr.Quantity = sSoLuong;
            pr.Short_Description = sMotaNgan;
            pr.Description = sMoTa;
            pr.Categories = cat.IDDanhMuc;             
            pr.Product_tag = sTag;
         

        

            



        string fileName = Path.GetFileNameWithoutExtension(img.ImageFile.FileName);
            string extention = Path.GetExtension(img.ImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extention;
            img.ImagePath = "~/Images/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Images/"), fileName);
            img.ImageFile.SaveAs(fileName);
            if (fileName != null)
            {
                pr.Thumbnail = img.ImagePath;
            }
            
                db.PRODUCTs.Add(pr);
                db.SaveChanges();
                return View(collection);

            
        }
        private QLBHEntities context = null;
        public bool KiemTraSP(PRODUCT pr, String Productname)
        {
            return context.PRODUCTs.Count(x => x.ProductName == Productname) > 0;
        }
        //public ActionResult SanPham(int count = 20)
        //{
        //    var productsList = (from product in db.PRODUCTs.Take(10)
        //                        select product);
        //    return View(productsList.ToList());
        //}

        public ActionResult SanPhamAdmin(int? page)
        {
            var model = db.PRODUCTs;
            int pageSize = 9;
            int pageNumber = (page ?? 1);
            return PartialView("SanPhamAdmin",model.ToList().OrderBy(n => n.ProductID).ToPagedList(pageNumber, pageSize));
        }
        public ActionResult DanhSachSanPhamAdmin()
        {
            return View();
        }
        // Sửa sản phẩm
        public ActionResult Edit(int ProductID)
        {
            var pr = db.PRODUCTs.First(m => m.ProductID == ProductID);
            return View(pr);
        }
        [HttpPost]
        public ActionResult Edit(int ProductID, FormCollection collection, PRODUCT PRO, Image1 img)
        {
            var pr = db.PRODUCTs.First(m => m.ProductID == ProductID);

            string sTenSP = collection["ProductName"];
            int sDonGia = int.Parse(collection["Price"]);
            int sGiasale = int.Parse(collection["Sale_Price"]);
            string sMotaNgan = collection["Short_Description"];
            string sMoTa = collection["Description"];
            string sTag = collection["Product_tag"];
           
            int sSoLuong = int.Parse(collection["Quantity"]);
           
            if (pr.Thumbnail != img.ImagePath)
            {
                string fileName = Path.GetFileNameWithoutExtension(img.ImageFile.FileName);
                string extention = Path.GetExtension(img.ImageFile.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extention;
                img.ImagePath = "~/Images/" + fileName;
                fileName = Path.Combine(Server.MapPath("~/Images/"), fileName);
                img.ImageFile.SaveAs(fileName);
                pr.Thumbnail = img.ImagePath;
            }
            
            
            


            pr.ProductName = sTenSP;
            pr.Price = sDonGia;
            pr.Sale_Price = sGiasale;
            pr.Quantity = sSoLuong;
            pr.Short_Description = sMotaNgan;
            pr.Description = sMoTa;
         
            pr.Product_tag = sTag;

          
           

            // image

            UpdateModel(pr);

            db.SaveChanges();
            return RedirectToAction("DanhSachSanPhamAdmin", "AdminSP");
        }
        public ActionResult Xoa(int ProductID)
        {
            PRODUCT pr = db.PRODUCTs.SingleOrDefault(n => n.ProductID == ProductID);
            if (pr == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            db.PRODUCTs.Remove(pr);
            db.SaveChanges();




            return RedirectToAction("DanhSachSanPhamAdmin", "AdminSP");

            // xoa trong table product ,
            // time kiem hinh anh co id san pham do trong table image va xoa no

        }

      
      

    }
}