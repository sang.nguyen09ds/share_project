﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testWEb.Models;


namespace testWEb.Controllers.Admin
{
  
    [Authorize(Roles = "Admin")]
    public class AdminDanhMucController : Controller
    {
        // GET: AdminDanhMuc
        QLBHEntities db = new QLBHEntities();


        [HttpGet]
        public ActionResult ThemDanhMuc()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ThemDanhMuc(FormCollection collection, PR_CATEGORIES dm)
        {
            string sTenDM = collection["TenDM"];
            dm.TenDanhMuc = sTenDM;
            if(dm.IDDanhMuc ==0)
            {
                dm.ParentID = null;
            }
            else
            {
                dm.ParentID = dm.IDDanhMuc;
            }
            db.PR_CATEGORIES.Add(dm);
            db.SaveChanges();
            return RedirectToAction("ThemDanhMuc", "AdminDanhMuc");
        }


        public ActionResult DanhMucAdmin()
        {
            CategoryModel cate = new CategoryModel();
            cate.catModel = db.PR_CATEGORIES.ToList();
            return View(cate);
        }
        
        public ActionResult ShowDMAdmin()
        {
            var model = db.PR_CATEGORIES;
            return PartialView("ShowDMAdmin", model);
        }
        public ActionResult XoaDM(int IDDanhMuc)
        {
            PR_CATEGORIES cat = db.PR_CATEGORIES.SingleOrDefault(x => x.IDDanhMuc == IDDanhMuc);
            if (cat == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            db.PR_CATEGORIES.Remove(cat);
            db.SaveChanges();
            return RedirectToAction("ThemDanhMuc", "AdminDanhMuc");


        }
    }


}