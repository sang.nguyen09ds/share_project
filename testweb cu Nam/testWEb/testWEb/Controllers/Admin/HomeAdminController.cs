﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using testWEb.Models;
using testWEb.Models.Admin;

namespace testWEb.Controllers.Admin
{
   
    [Authorize(Roles = "Admin")]
    public class HomeAdminController : Controller
    {
        QLBHEntities db = new QLBHEntities();

        // GET: HomeAdmin
        
        public ActionResult Index()
        {
            var userCookie = GetUserCookie();
            if (userCookie != null)
            {
                ViewBag.User = userCookie;
            }
            return View();
        }

        private object GetUserCookie()
        {
            string cookieName = FormsAuthentication.FormsCookieName; //Find cookie name
            HttpCookie authCookie = HttpContext.Request.Cookies[cookieName]; //Get the cookie by it's name

            if (authCookie != null)
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value); //Decrypt it
                var UserName = ticket.Name; //You have the UserName!
                if (UserName != null)
                {
                    return UserName;
                }

            }

            return null;
        }

        [HttpGet]
        public ActionResult PageGioiThieu()
        {
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PageGioiThieu(FormCollection collection, ImagePost img, GioiThieu gt)
        {
            
            string sNoiDung = collection["NoiDung"];
            string sTieuDe = collection["TieuDe"];

            gt.TieuDe = sTieuDe;
            gt.NoiDung = sNoiDung;
           

            string fileName = Path.GetFileNameWithoutExtension(img.Thumbnail.FileName);
            string extention = Path.GetExtension(img.Thumbnail.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extention;
            img.ImagePath = "~/Images/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Images/"), fileName);
            img.Thumbnail.SaveAs(fileName);
            if (fileName != null)
            {
                gt.HinhAnh = img.ImagePath;
            }

            db.GioiThieux.Add(gt);
            db.SaveChanges();
            return View(collection);


        }
    }
}