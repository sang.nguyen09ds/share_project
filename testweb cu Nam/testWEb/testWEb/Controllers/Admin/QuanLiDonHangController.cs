﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testWEb.Models;
namespace testWEb.Controllers.Admin
{
  
    [Authorize(Roles = "Admin")]
    public class QuanLiDonHangController : Controller
    {
        QLBHEntities db = new QLBHEntities();
        public ActionResult DonHang()
        {
            var model = db.DonHangs;
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult UpdateOrder(FormCollection orderDeatailForm)
        {
            int ID = int.Parse(orderDeatailForm["IDDonHang"]);
            DonHang dh = db.DonHangs.First(m => m.ID == ID);
            dh.TinhTrang = 2;
            UpdateModel(dh);
            db.SaveChanges();
            return View(dh);
        }
            public ActionResult CTDonHang(int ID)
        {
            var model = (from A in db.DonHangs join B in db.ChitietDonHangs on A.ID equals B.MaDonHang
                         join C in db.PRODUCTs on B.IDSanPham equals C.ProductID
                         join D in db.CUSTOMERs on A.MaKhachHang equals D.ID
                         where B.MaDonHang == ID
                        select new DonHangViewModel
                        {
                            IDDH = B.MaDonHang,
                            NgayTao = A.NgayTao,
                            IDSP = B.IDSanPham,
                            IDKH = A.MaKhachHang,
                            TenSP = C.ProductName,
                            HoKH = D.Ho,
                            TenKH = D.Ten,
                            Email = D.Email,
                            SoLuong = B.SoLuong,
                            DonGia = B.Gia,
                            ThanhTien = B.TongTien,
                            TinhTrang = A.TinhTrang
                        });
            ViewBag.IdDonHang = ID;
            return PartialView(model);
        }
        public ActionResult QuanLi_DH_CTDH()
        {

            return View();

        }
        public ActionResult Xoa(int ID)
        {
            DonHang dh = db.DonHangs.SingleOrDefault(n => n.ID == ID);
            if (dh == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            db.DonHangs.Remove(dh);
            db.SaveChanges();

            return RedirectToAction("QuanLi_DH_CTDH", "QuanLiDonHang");

            // xoa trong table product ,
            // time kiem hinh anh co id san pham do trong table image va xoa no

        }
        public ActionResult XoaCTDH(int ID)
        {
            ChitietDonHang ctdh = db.ChitietDonHangs.SingleOrDefault(n => n.ID == ID);
            if (ctdh == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            db.ChitietDonHangs.Remove(ctdh);
            db.SaveChanges();

            return RedirectToAction("QuanLi_DH_CTDH", "QuanLiDonHang");
            // xoa trong table product ,

        }
    }
}