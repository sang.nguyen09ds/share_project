﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testWEb.Models;


namespace testWEb.Controllers
{
    public class giohangController : Controller
    {
        QLBHEntities db = new QLBHEntities();
        
        public ActionResult CartView()
        {
           
            return View();
        }
        public ActionResult ViewBagGioHang()
        {
            ViewBag.TongSoLuong = TongSoLuong();
            ViewBag.TongTien = TongTien();
            return View();
        }
        public ActionResult Cart()
        {
            List<ShoppingCartItem> giohang = Session["giohang"] as List<ShoppingCartItem>;
            return View(giohang);
        }
        public RedirectToRouteResult ThemVaoGio(int SanPhamID)
        {
            if (Session["giohang"] == null) // Nếu giỏ hàng chưa được khởi tạo
            {
                Session["giohang"] = new List<ShoppingCartItem>();  // Khởi tạo Session["giohang"] là 1 List<CartItem>
            }

            List<ShoppingCartItem> giohang = Session["giohang"] as List<ShoppingCartItem>;  // Gán qua biến giohang dễ code

            // Kiểm tra xem sản phẩm khách đang chọn đã có trong giỏ hàng chưa

            if (giohang.FirstOrDefault(m => m.ProductID == SanPhamID) == null) // ko co sp nay trong gio hang
            {
                PRODUCT sp = db.PRODUCTs.Find(SanPhamID);  // tim sp theo sanPhamID

                ShoppingCartItem newItem = new ShoppingCartItem()
                {
                    ProductID = SanPhamID,
                    ProductName = sp.ProductName,
                    Quanlity = 1,
                    Hinh = sp.Thumbnail,
                    Price  = sp.Price

                };  // Tạo ra 1 CartItem mới
                giohang.Add(newItem);  // Thêm CartItem vào giỏ 
               
            }
            else
            {
                // Nếu sản phẩm khách chọn đã có trong giỏ hàng thì không thêm vào giỏ nữa mà tăng số lượng lên.
                ShoppingCartItem cardItem = giohang.FirstOrDefault(m => m.ProductID == SanPhamID);
                cardItem.Quanlity++;
            }

            // Action này sẽ chuyển hướng về trang chi tiết sp khi khách hàng đặt vào giỏ thành công. Bạn có thể chuyển về chính trang khách hàng vừa đứng bằng lệnh return Redirect(Request.UrlReferrer.ToString()); nếu muốn.
            return RedirectToAction("CartView", "giohang", new { id = SanPhamID });
        }
        public RedirectToRouteResult SuaSoLuong(int SanPhamID, int soluongmoi)
        {
            // tìm carditem muon sua
            List<ShoppingCartItem> giohang = Session["giohang"] as List<ShoppingCartItem>;
            ShoppingCartItem itemSua = giohang.FirstOrDefault(m => m.ProductID == SanPhamID);
            if (itemSua != null)
            {
                itemSua.Quanlity = soluongmoi;
            }
            return RedirectToAction("CartView", "giohang", new { id = SanPhamID });

        }
        public RedirectToRouteResult XoaKhoiGio(int SanPhamID)
        {
            List<DonHang> giohang = Session["giohang"] as List<DonHang>;
            DonHang itemXoa = giohang.FirstOrDefault(m => m.ID == SanPhamID);
            if (itemXoa != null)
            {
                giohang.Remove(itemXoa);
            }
            return RedirectToAction("CartView", "giohang", new { id = SanPhamID });
        }
        public double TongTien()
        {
            double dTongTien = 0;
            List<ShoppingCartItem> lsGioHang = Session["giohang"] as List<ShoppingCartItem>;
            if(lsGioHang !=null)
            {
                dTongTien = lsGioHang.Sum(n => n.Total);
            }
            return dTongTien;
        }
        public double TongSoLuong()
        {
            double dTongSoLuong = 0;
            List<ShoppingCartItem> lsGioHang = Session["giohang"] as List<ShoppingCartItem>;
            if (lsGioHang != null)
            {
                dTongSoLuong = (int)lsGioHang.Sum(n => n.Quanlity);

            }
            return dTongSoLuong;
        }
        public ActionResult ViewitemGH()
        {
            if (TongSoLuong() == 0)
            {
                return PartialView();
            }

            return PartialView();
        }
        [HttpGet]
        public ActionResult DatHang(PRODUCT pr)
        {
            List<ShoppingCartItem> giohang = Session["giohang"] as List<ShoppingCartItem>;
            return View(giohang);
        }
        
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult DatHang(FormCollection collection, ChitietDonHang orderDetail , CUSTOMER cus,PRODUCT pr,DonHang dh)
        {

            //1. lưu thong tin khach hang
            // 1.1 Kiem tra email khach hang da ton tai chua ? 
            // chua -> add new , 
            // roi thì update
            // Table : KhachHang

            string sHo = collection["txtHo"];
            string sTen = collection["txtTen"];
            string sEmail = collection["txtEmail"];
            int sSDT = int.Parse(collection["txtSDT"]);
            string sDiaChi = collection["txtDiaChi"];
            string sTP = collection["txtTP"];
            string sTK = collection["txtTK"];

            cus.Ho = sHo;
            cus.Ten = sTen;
            cus.Email = sEmail;
            cus.SDT = sSDT;
            cus.DiaChi = sDiaChi;
            cus.Tinh_TP = sTP;
            cus.TaiKhoan = sTK;
            
            db.CUSTOMERs.Add(cus);
            db.SaveChanges();



            // lay Ma khach sau khi SaveChanges
            dh.MaKhachHang = cus.ID;
            dh.NgayTao = DateTime.Today.ToString("dd/MM/yyyy");
            dh.TinhTrang = 1;
            db.DonHangs.Add(dh);
            db.SaveChanges();

            // 2. Lưu gio hang vao table Chi tiet don hang
            // 2.1 Lay Session["giohang"]
            // thong tin can 
            // - Ma Khach Hang

            List<ShoppingCartItem> giohang = Session["giohang"] as List<ShoppingCartItem>;

            foreach(var item in giohang)
            {
                orderDetail.SoLuong = item.Quanlity;
                orderDetail.IDSanPham = item.ProductID;
                orderDetail.MaDonHang = dh.ID;
                orderDetail.Gia = item.Price;
                orderDetail.TongTien = item.Total;
                db.ChitietDonHangs.Add(orderDetail);
                db.SaveChanges();
            }
            Session["giohang"] = null;
            // xong het thi chuyen ve trang thông báo đăt hàng thành công , cảm ơn ...
            return RedirectToAction("DatHangThanhCong",collection);

        }
        public ActionResult DatHangThanhCong()
        {
            return View();
        }
       
    }
    
}