﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testWEb.Models;

namespace testWEb.Controllers
{
    public class TrangChuController : Controller
    {
        QLBHEntities db = new QLBHEntities();

        [HandleError]
        public ActionResult Index()
        {
            ViewData["uname"] = User.Identity.Name;
            var model = db.PRODUCTs;
           
            return PartialView("Index", model);
        }

        

        public ActionResult LienHe()
        {
            return View();
        }
        public ActionResult GioiThieu()
        {
            var model = db.GioiThieux;

            return PartialView("GioiThieu", model);
        }
        public ActionResult ViewGioiThieu()
        {
            List<GioiThieu> post = db.GioiThieux.ToList();
            return PartialView("ViewGioiThieu", post);
        }

    }
}