﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testWEb.Models;
using PagedList;
using PagedList.Mvc;
namespace testWEb.Controllers
{
    public class ProductController : Controller
    {

        QLBHEntities db = new QLBHEntities();
        
        public ActionResult DanhSachSanPham(int? page)
        {
            var productsList = db.PRODUCTs;
            int pageSize = 9;
            int pageNumber = (page ?? 1);
            return View(productsList.ToList().OrderBy(n => n.ProductID).ToPagedList(pageNumber, pageSize));
        }

        public ActionResult SanPham()
        {
            var model = db.PRODUCTs;
            return PartialView("SanPham", model);
        }
         public PRODUCT Detail(int ID)
        {
            QLBHEntities db = new QLBHEntities();
            return db.PRODUCTs.Find(ID);
        }

        public ActionResult DanhMucSanPhamCha(int ID)
        {
            List<PR_CATEGORIES> ls = new List<PR_CATEGORIES>();
            ls = db.PR_CATEGORIES.Where(n => n.ParentID == ID).ToList();
            ViewBag.Count = ls.Count();
            return PartialView("DanhMucSanPhamCha",ls);

        }



        public ActionResult DanhMucSanPham()
        {
          var ls = db.PR_CATEGORIES.Where(n => n.ParentID == null).ToList();
           return View(ls);

        }
        public ActionResult DanhSachSPtheoDM(int ID)
        { 
            return View(db.PRODUCTs.Where(x=>x.Categories==ID));
        }
        public ActionResult Menu()
        {
            List<PR_CATEGORIES> cat_list = db.PR_CATEGORIES.ToList();
            //return PartialView("Tên parital",đối tượng);
            return PartialView("Menu", cat_list);
        }
        public ActionResult ChiTietSP(int ID)
        {
            List<PRODUCT> pr = db.PRODUCTs.Where(x => x.ProductID == ID).ToList();
            
            return PartialView("ChiTietSP", pr);
        }


    }
}