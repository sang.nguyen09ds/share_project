﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testWEb.Models;
using PagedList;
using PagedList.Mvc;
namespace testWEb.Controllers
{
    public class TimKiemController : Controller
    {
        QLBHEntities db = new QLBHEntities();
        // GET: TimKiem
        [HttpGet]
        public ActionResult TimKiem(string sTuKhoa, int? page)
        {
            List<PRODUCT> pr = db.PRODUCTs.Where(n => n.ProductName.Contains(sTuKhoa)).ToList();
            ViewBag.TuKhoa = sTuKhoa;
            // Phân trang
            int pageNumber = (page ?? 1);
            int pageSize = 9;

            if (sTuKhoa != null)
            {
               
                ViewBag.ThongBao = "Đã tìm thấy ' " + pr.Count + " ' kết quả!";
                return View(pr.ToPagedList(pageNumber, pageSize));
            }
            ViewBag.ThongBao = "Không tìm thấy sản phẩm nào !";
            return View(db.PRODUCTs.OrderBy(n => n.ProductName).ToPagedList(pageNumber, pageSize));
        }
        [HttpPost]
        public ActionResult TimKiem(FormCollection collection, int? page)
        {

            string sTuKhoa = collection["txtTimKiem"].ToString();
            ViewBag.TuKhoa = sTuKhoa;
            List<PRODUCT> pr = db.PRODUCTs.Where(n => n.ProductName.Contains(sTuKhoa)).ToList();
            // Phân trang

            int pageNumber = (page ?? 1);
            int pageSize = 9;
            if (pr.Count == 0)
            {
                ViewBag.ThongBao = "Không tìm thấy sản phẩm nào";

                return View(db.PRODUCTs.OrderBy(n => n.ProductName).ToPagedList(pageNumber, pageSize));
            }
            else
            {
                if (sTuKhoa != null)
                {
                    var model = db.PRODUCTs.OrderBy(n => n.ProductName == sTuKhoa);
                    ViewBag.ThongBao = "Đã tìm thấy ' " + pr.Count + " ' kết quả!";
                    return View(model.ToPagedList(pageNumber, pageSize));
                }
                if (sTuKhoa != null)
                {
                    var model = db.PRODUCTs.OrderBy(n => n.Product_tag == sTuKhoa);
                    ViewBag.ThongBao = "Đã tìm thấy ' " + pr.Count + " ' kết quả!";
                    return View(model.ToPagedList(pageNumber, pageSize));
                }

                if (sTuKhoa != null)
                {
                    var model = db.PRODUCTs.OrderBy(n => n.Price == int.Parse(sTuKhoa));
                    ViewBag.ThongBao = "Đã tìm thấy ' " + pr.Count + " ' kết quả!";
                    return View(model.ToPagedList(pageNumber, pageSize));
                }

                if (sTuKhoa != null)
                {
                    var model = db.PRODUCTs.OrderBy(n => n.Categories == int.Parse(sTuKhoa));
                    ViewBag.ThongBao = "Đã tìm thấy ' " + pr.Count + " ' kết quả!";
                    return View(model.ToPagedList(pageNumber, pageSize));
                }

            }
            return View();
        }

    }
}