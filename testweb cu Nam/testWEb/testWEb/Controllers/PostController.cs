﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testWEb.Models;
using PagedList;
using PagedList.Mvc;
namespace testWEb.Controllers
{
    public class PostController : Controller
    {
        QLBHEntities db = new QLBHEntities();
        // GET: Post
        public ActionResult Post(int ID)
        {
            List<POST> post = db.POSTs.Where(x => x.Id == ID).ToList();
            return PartialView("Post", post);
        }
        public ActionResult DanhSachPost(int? page)
        {
            var model = db.POSTs;
            int pageSize = 9;
            int pageNumber = (page ?? 1);
            return View(model.ToList().OrderBy(n=>n.Id).ToPagedList(pageNumber, pageSize));
        }
        public ActionResult DanhMucPost()
        {
            List<POST> post = db.POSTs.ToList();
            return PartialView("DanhMucPost", post);
        }
        public ActionResult DanhSachPostAdmin()
        {

            return View();
        }
        public ActionResult ViewPost()
        {
            List<POST> post = db.POSTs.ToList();
            return PartialView("ViewPost", post);
        }
    }
}