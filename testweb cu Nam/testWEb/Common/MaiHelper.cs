﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace Common
{
    public class MailHelper
    {
        public void SendMail(string subject, string content, string toEmailAdress)
        {
            var fromEmailAdress = ConfigurationManager.AppSettings["FormEmailAddress"].ToString();
            var formEmailDisplayName = ConfigurationManager.AppSettings["FormEmailDisplayName"].ToString();
            var formEmailPassword = ConfigurationManager.AppSettings["FormEmailPassword"].ToString();
            var sMTHost = ConfigurationManager.AppSettings["SMTHost"].ToString();
            var sMTPort = ConfigurationManager.AppSettings["SMTPort"].ToString();



            bool enabledSSL = bool.Parse(ConfigurationManager.AppSettings["EnabledSSL"].ToString());

            string body = content;
            MailMessage message = new MailMessage(new MailAddress(fromEmailAdress, formEmailDisplayName), new MailAddress(toEmailAdress));
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = content;

            var client = new SmtpClient();
            client.Credentials = new NetworkCredential(fromEmailAdress, formEmailPassword);
            client.Host = sMTHost;
            client.EnableSsl = enabledSSL;
            client.Port = !string.IsNullOrEmpty(sMTPort)? Convert.ToInt32(sMTPort):0;
            client.Send(message);
        }
    }
}
